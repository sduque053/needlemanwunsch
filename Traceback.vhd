library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
LIBRARY lpm;
USE lpm.lpm_components.all;

entity Traceback is
	port(
		CLK		: IN  std_logic;
		TB 		: IN  std_logic_vector(31 downto  0);
		S1,S2		: IN  std_logic_vector( 7 downto  0);
		A1,A2		: OUT std_logic_vector(20 downto  0);
		OS1,OS2	: OUT std_logic_vector( 7 downto  0);
		ODIR		: OUT	std_logic_vector(31 downto  0)
	);
end entity;

architecture RTL of Traceback is
	
	signal CLKP,P	: std_logic;
	signal QD,DD	: std_logic_vector(31 downto 0):=(others=>'0');
	signal QS1,DS1	: std_logic_vector( 7 downto 0):=(others=>'0');
	signal QS2,DS2	: std_logic_vector( 7 downto 0):=(others=>'0');
	signal QA1,DA1	: std_logic_vector(20 downto 0):=(others=>'0');
	signal QA2,DA2	: std_logic_vector(20 downto 0):=(others=>'0');

	begin
	
	with QD(31 downto 30) select DD<=
		TB 									when "00",
		QD(29 downto 0)&"00" 			when "10",
		QD(23 downto 0)&"00000000"		when "01",
		QD(21 downto 0)&"0000000000"	when "11";
	
	with QD(31 downto 30) select DS1<=
		S1							when "00",
		QS1						when "01",
		QS1(5 downto 0)&"00" when "10",
		QS1(5 downto 0)&"00" when "11";
		
	with QD(31 downto 30) select DS2<=
		S2							when "00",
		QS2						when "10",
		QS2(5 downto 0)&"00" when "01",
		QS2(5 downto 0)&"00" when "11";
		
	with QD(31) select DA1<=
		'0'&QS1(7 downto 6)&QA1(20 downto 3) when '1',
		"100"&QA1(20 downto 3)					when '0';
		
	with QD(30) select DA2<=
		'0'&QS2(7 downto 6)&QA2(20 downto 3)	when '1',
		"100"&QA2(20 downto 3)					when '0';
		
	
	REGDIR:lpm_ff
		generic map(
			lpm_fftype 	=> "DFF",
			lpm_width	=> 32
		)
		port map(
			clock	=> CLK,
			data	=> DD,
			q		=> QD
		);
			
	REGS1:lpm_ff
		generic map(
			lpm_fftype 	=> "DFF",
			lpm_width	=> 8
		)
		port map(
			clock	=> CLK,
			data	=> DS1,
			q		=> QS1
		);
	
	REGS2:lpm_ff
		generic map(
			lpm_fftype 	=> "DFF",
			lpm_width	=> 8
		)
		port map(
			clock	=> CLK,
			data	=> DS2,
			q		=> QS2
		);
		
	P<=QD(31) or QD(30);
	CLKP<=CLK and P;
	
	REGA1:lpm_ff
		generic map(
			lpm_fftype 	=> "DFF",
			lpm_width	=> 21
		)
		port map(
			clock	=> CLKP,
			data	=> DA1,
			q		=> QA1
		);
	
	REGA2:lpm_ff
		generic map(
			lpm_fftype 	=> "DFF",
			lpm_width	=> 21
		)
		port map(
			clock	=> CLKP,
			data	=> DA2,
			q		=> QA2
		);
	
	A1<=QA1;
	A2<=QA2;
	OS1<=QS1;
	OS2<=QS2;
	ODIR<=QD;
end RTL;
	
	
	
	
		
		