library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.math_real.all;
LIBRARY lpm;
USE lpm.lpm_components.all;

entity PE is
  generic(
	N :positive:=4;
	W :integer(ceil(log2(real(N))))
	);
  port(
	CLK			:IN  std_logic;
	S1,S2		:IN  std_logic_vector(1 downto 0);
	D0,D1,D2	:IN  std_logic_vector(W-1 downto 0);
	DIR			:OUT std_logic_vector(1 downto 0);
	D3			:OUT std_logic_vector(W-1 downto 0)
	);
end entity;

architecture RTL of PE is

  signal RD,RQ				:std_logic_vector(W+1 downto 0);
  signal AA1,AB1,AS1 		:std_logic_vector(W-1 downto 0);
  signal AA2,AB2,AS2 		:std_logic_vector(W-1 downto 0);
  signal MUX2					:std_logic_vector(W-1 downto 0);
  signal DIC					:std_logic_vector(1 downto 0);
  signal S1ES2,D2LD1,FL	:std_logic;


begin

  COMP1: lpm_compare
	generic map(
	  lpm_width				=>2,
	  lpm_representation	=>"signed"
	  )
	port map(
	  dataa		=>S1,
	  datab		=>S2,
	  aeb		=>S1ES2

	  );

  with S1ES2 select AA1<=
	"1111" when '0',
	"0001" when '1';

  AB1<=D0;

  ADD1: lpm_add_sub
	generic map(
	  lpm_width 				=>4,
	  lpm_representation	=>"signed"
	  )
	port map(
	  dataa 	=>AA1,
	  datab 	=>AB1,
	  result	=>AS1
	  );


  COMP2: lpm_compare
	generic map(
	  lpm_width 				=>4,
	  lpm_representation	=>"signed"
	  )
	port map(
	  dataa		=>D2,
	  datab		=>D1,
	  aleb		=>D2LD1
	  );



  with D2LD1	select AA2<=
	D2 when '0',
	D1 when '1';

  with D2LD1 select DIC<=
	"01" when '1',
	"10" when '0';

  AB2<="1111";

  ADD2: lpm_add_sub
	generic map(
	  lpm_width 				=>4,
	  lpm_representation	=>"signed"
	  )
	port map(
	  dataa 	=>AA2,
	  datab 	=>AB2,
	  result	=>AS2
	  );


  COMP3: lpm_compare
	generic map(
	  lpm_width				=>4,
	  lpm_representation	=>"signed"
	  )
	port map(
	  dataa		=>AS2,
	  datab		=>AS1,
	  aleb		=>FL
	  );


  with FL select RD(3 downto 0)<=
	AS2 when '0',
	AS1 when '1';

  with FL select RD(5 downto 4)<=
	DIC  when '0',
	"11" when '1';

  REG: lpm_ff
	generic map(
	  lpm_width => 6
	  )
	port map(
	  clock => CLK,
	  data	=>RD,
	  q		=>RQ
	  );

  DIR<=RQ(5 downto 4);
  D3	<=RQ(3 downto 0);


end RTL;
