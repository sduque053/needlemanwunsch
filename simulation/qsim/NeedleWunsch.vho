-- Copyright (C) 2018  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 18.1.0 Build 625 09/12/2018 SJ Lite Edition"

-- DATE "09/06/2020 12:47:33"

-- 
-- Device: Altera 5CGXFC7C7F23C8 Package FBGA484
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY ALTERA_LNSIM;
LIBRARY CYCLONEV;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE ALTERA_LNSIM.ALTERA_LNSIM_COMPONENTS.ALL;
USE CYCLONEV.CYCLONEV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	Traceback IS
    PORT (
	CLK : IN std_logic;
	TB : IN std_logic_vector(31 DOWNTO 0);
	S1 : IN std_logic_vector(7 DOWNTO 0);
	S2 : IN std_logic_vector(7 DOWNTO 0);
	A1 : OUT std_logic_vector(20 DOWNTO 0);
	A2 : OUT std_logic_vector(20 DOWNTO 0);
	OS1 : OUT std_logic_vector(7 DOWNTO 0);
	OS2 : OUT std_logic_vector(7 DOWNTO 0);
	ODIR : OUT std_logic_vector(31 DOWNTO 0)
	);
END Traceback;

-- Design Ports Information
-- A1[0]	=>  Location: PIN_M8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A1[1]	=>  Location: PIN_AB15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A1[2]	=>  Location: PIN_T13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A1[3]	=>  Location: PIN_AB20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A1[4]	=>  Location: PIN_E16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A1[5]	=>  Location: PIN_AA13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A1[6]	=>  Location: PIN_P8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A1[7]	=>  Location: PIN_AA14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A1[8]	=>  Location: PIN_AA15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A1[9]	=>  Location: PIN_AA18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A1[10]	=>  Location: PIN_Y14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A1[11]	=>  Location: PIN_P12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A1[12]	=>  Location: PIN_E15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A1[13]	=>  Location: PIN_T12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A1[14]	=>  Location: PIN_AA20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A1[15]	=>  Location: PIN_R16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A1[16]	=>  Location: PIN_AB21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A1[17]	=>  Location: PIN_N6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A1[18]	=>  Location: PIN_U7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A1[19]	=>  Location: PIN_H16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A1[20]	=>  Location: PIN_A15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A2[0]	=>  Location: PIN_L18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A2[1]	=>  Location: PIN_AB11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A2[2]	=>  Location: PIN_Y15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A2[3]	=>  Location: PIN_A14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A2[4]	=>  Location: PIN_Y16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A2[5]	=>  Location: PIN_B13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A2[6]	=>  Location: PIN_T7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A2[7]	=>  Location: PIN_U10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A2[8]	=>  Location: PIN_R5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A2[9]	=>  Location: PIN_W8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A2[10]	=>  Location: PIN_T8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A2[11]	=>  Location: PIN_AA8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A2[12]	=>  Location: PIN_AB7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A2[13]	=>  Location: PIN_Y17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A2[14]	=>  Location: PIN_V14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A2[15]	=>  Location: PIN_F15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A2[16]	=>  Location: PIN_T9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A2[17]	=>  Location: PIN_T17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A2[18]	=>  Location: PIN_N8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A2[19]	=>  Location: PIN_AA17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A2[20]	=>  Location: PIN_M20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OS1[0]	=>  Location: PIN_R11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OS1[1]	=>  Location: PIN_L17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OS1[2]	=>  Location: PIN_R22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OS1[3]	=>  Location: PIN_P18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OS1[4]	=>  Location: PIN_L22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OS1[5]	=>  Location: PIN_Y11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OS1[6]	=>  Location: PIN_T19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OS1[7]	=>  Location: PIN_T20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OS2[0]	=>  Location: PIN_AB5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OS2[1]	=>  Location: PIN_R17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OS2[2]	=>  Location: PIN_P16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OS2[3]	=>  Location: PIN_N16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OS2[4]	=>  Location: PIN_M21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OS2[5]	=>  Location: PIN_V9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OS2[6]	=>  Location: PIN_W16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OS2[7]	=>  Location: PIN_T18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ODIR[0]	=>  Location: PIN_W21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ODIR[1]	=>  Location: PIN_AA22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ODIR[2]	=>  Location: PIN_T22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ODIR[3]	=>  Location: PIN_T14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ODIR[4]	=>  Location: PIN_U20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ODIR[5]	=>  Location: PIN_M9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ODIR[6]	=>  Location: PIN_P22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ODIR[7]	=>  Location: PIN_P6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ODIR[8]	=>  Location: PIN_P14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ODIR[9]	=>  Location: PIN_AA7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ODIR[10]	=>  Location: PIN_V19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ODIR[11]	=>  Location: PIN_AB10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ODIR[12]	=>  Location: PIN_Y21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ODIR[13]	=>  Location: PIN_U15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ODIR[14]	=>  Location: PIN_U16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ODIR[15]	=>  Location: PIN_Y10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ODIR[16]	=>  Location: PIN_U17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ODIR[17]	=>  Location: PIN_Y19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ODIR[18]	=>  Location: PIN_U22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ODIR[19]	=>  Location: PIN_AB22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ODIR[20]	=>  Location: PIN_P19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ODIR[21]	=>  Location: PIN_P7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ODIR[22]	=>  Location: PIN_U21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ODIR[23]	=>  Location: PIN_V16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ODIR[24]	=>  Location: PIN_K17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ODIR[25]	=>  Location: PIN_Y22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ODIR[26]	=>  Location: PIN_N19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ODIR[27]	=>  Location: PIN_V13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ODIR[28]	=>  Location: PIN_V21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ODIR[29]	=>  Location: PIN_R14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ODIR[30]	=>  Location: PIN_W22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ODIR[31]	=>  Location: PIN_Y20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S1[0]	=>  Location: PIN_M18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- CLK	=>  Location: PIN_M16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S1[1]	=>  Location: PIN_H18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S1[2]	=>  Location: PIN_P9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S1[3]	=>  Location: PIN_M22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S1[4]	=>  Location: PIN_B15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S1[5]	=>  Location: PIN_AB6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S1[6]	=>  Location: PIN_L19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S1[7]	=>  Location: PIN_R15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S2[0]	=>  Location: PIN_R7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S2[1]	=>  Location: PIN_R9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S2[2]	=>  Location: PIN_M7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S2[3]	=>  Location: PIN_V10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S2[4]	=>  Location: PIN_G18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S2[5]	=>  Location: PIN_U12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S2[6]	=>  Location: PIN_M6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S2[7]	=>  Location: PIN_K22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- TB[0]	=>  Location: PIN_V6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- TB[1]	=>  Location: PIN_AB17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- TB[2]	=>  Location: PIN_V20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- TB[3]	=>  Location: PIN_AA10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- TB[4]	=>  Location: PIN_AA19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- TB[5]	=>  Location: PIN_U11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- TB[6]	=>  Location: PIN_T15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- TB[7]	=>  Location: PIN_R10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- TB[8]	=>  Location: PIN_J18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- TB[9]	=>  Location: PIN_W9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- TB[10]	=>  Location: PIN_U13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- TB[11]	=>  Location: PIN_AA9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- TB[12]	=>  Location: PIN_J19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- TB[13]	=>  Location: PIN_AB18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- TB[14]	=>  Location: PIN_AB13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- TB[15]	=>  Location: PIN_T10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- TB[16]	=>  Location: PIN_N9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- TB[17]	=>  Location: PIN_U6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- TB[18]	=>  Location: PIN_R21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- TB[19]	=>  Location: PIN_R12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- TB[20]	=>  Location: PIN_W19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- TB[21]	=>  Location: PIN_AB8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- TB[22]	=>  Location: PIN_AA12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- TB[23]	=>  Location: PIN_Y9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- TB[24]	=>  Location: PIN_N20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- TB[25]	=>  Location: PIN_R6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- TB[26]	=>  Location: PIN_K21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- TB[27]	=>  Location: PIN_N21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- TB[28]	=>  Location: PIN_V15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- TB[29]	=>  Location: PIN_AB12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- TB[30]	=>  Location: PIN_V18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- TB[31]	=>  Location: PIN_P17,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF Traceback IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_CLK : std_logic;
SIGNAL ww_TB : std_logic_vector(31 DOWNTO 0);
SIGNAL ww_S1 : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_S2 : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_A1 : std_logic_vector(20 DOWNTO 0);
SIGNAL ww_A2 : std_logic_vector(20 DOWNTO 0);
SIGNAL ww_OS1 : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_OS2 : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_ODIR : std_logic_vector(31 DOWNTO 0);
SIGNAL \~QUARTUS_CREATED_GND~I_combout\ : std_logic;
SIGNAL \CLK~input_o\ : std_logic;
SIGNAL \CLK~inputCLKENA0_outclk\ : std_logic;
SIGNAL \TB[7]~input_o\ : std_logic;
SIGNAL \TB[5]~input_o\ : std_logic;
SIGNAL \TB[1]~input_o\ : std_logic;
SIGNAL \TB[0]~input_o\ : std_logic;
SIGNAL \Mux31~0_combout\ : std_logic;
SIGNAL \TB[10]~input_o\ : std_logic;
SIGNAL \TB[6]~input_o\ : std_logic;
SIGNAL \TB[2]~input_o\ : std_logic;
SIGNAL \Mux29~0_combout\ : std_logic;
SIGNAL \TB[4]~input_o\ : std_logic;
SIGNAL \Mux27~0_combout\ : std_logic;
SIGNAL \Mux25~0_combout\ : std_logic;
SIGNAL \TB[8]~input_o\ : std_logic;
SIGNAL \Mux23~0_combout\ : std_logic;
SIGNAL \Mux21~0_combout\ : std_logic;
SIGNAL \TB[12]~input_o\ : std_logic;
SIGNAL \Mux19~0_combout\ : std_logic;
SIGNAL \TB[14]~input_o\ : std_logic;
SIGNAL \Mux17~0_combout\ : std_logic;
SIGNAL \TB[16]~input_o\ : std_logic;
SIGNAL \Mux15~0_combout\ : std_logic;
SIGNAL \TB[18]~input_o\ : std_logic;
SIGNAL \Mux13~0_combout\ : std_logic;
SIGNAL \TB[20]~input_o\ : std_logic;
SIGNAL \Mux11~0_combout\ : std_logic;
SIGNAL \TB[28]~input_o\ : std_logic;
SIGNAL \TB[22]~input_o\ : std_logic;
SIGNAL \Mux9~0_combout\ : std_logic;
SIGNAL \TB[24]~input_o\ : std_logic;
SIGNAL \Mux7~0_combout\ : std_logic;
SIGNAL \TB[26]~input_o\ : std_logic;
SIGNAL \Mux5~0_combout\ : std_logic;
SIGNAL \Mux3~0_combout\ : std_logic;
SIGNAL \TB[30]~input_o\ : std_logic;
SIGNAL \Mux1~0_combout\ : std_logic;
SIGNAL \Mux30~0_combout\ : std_logic;
SIGNAL \TB[3]~input_o\ : std_logic;
SIGNAL \Mux28~0_combout\ : std_logic;
SIGNAL \Mux26~0_combout\ : std_logic;
SIGNAL \Mux24~0_combout\ : std_logic;
SIGNAL \TB[9]~input_o\ : std_logic;
SIGNAL \Mux22~0_combout\ : std_logic;
SIGNAL \TB[17]~input_o\ : std_logic;
SIGNAL \TB[13]~input_o\ : std_logic;
SIGNAL \TB[11]~input_o\ : std_logic;
SIGNAL \Mux20~0_combout\ : std_logic;
SIGNAL \Mux18~0_combout\ : std_logic;
SIGNAL \TB[15]~input_o\ : std_logic;
SIGNAL \Mux16~0_combout\ : std_logic;
SIGNAL \Mux14~0_combout\ : std_logic;
SIGNAL \TB[19]~input_o\ : std_logic;
SIGNAL \Mux12~0_combout\ : std_logic;
SIGNAL \TB[21]~input_o\ : std_logic;
SIGNAL \Mux10~0_combout\ : std_logic;
SIGNAL \TB[27]~input_o\ : std_logic;
SIGNAL \TB[23]~input_o\ : std_logic;
SIGNAL \Mux8~0_combout\ : std_logic;
SIGNAL \TB[25]~input_o\ : std_logic;
SIGNAL \Mux6~0_combout\ : std_logic;
SIGNAL \Mux4~0_combout\ : std_logic;
SIGNAL \TB[29]~input_o\ : std_logic;
SIGNAL \Mux2~0_combout\ : std_logic;
SIGNAL \TB[31]~input_o\ : std_logic;
SIGNAL \Mux0~0_combout\ : std_logic;
SIGNAL \CLKP~combout\ : std_logic;
SIGNAL \S1[0]~input_o\ : std_logic;
SIGNAL \Mux39~0_combout\ : std_logic;
SIGNAL \REGS1|dffs[0]~0_combout\ : std_logic;
SIGNAL \S1[2]~input_o\ : std_logic;
SIGNAL \Mux37~0_combout\ : std_logic;
SIGNAL \S1[4]~input_o\ : std_logic;
SIGNAL \Mux35~0_combout\ : std_logic;
SIGNAL \S1[6]~input_o\ : std_logic;
SIGNAL \Mux33~0_combout\ : std_logic;
SIGNAL \DA1[18]~0_combout\ : std_logic;
SIGNAL \REGA1|dffs[12]~feeder_combout\ : std_logic;
SIGNAL \REGA1|dffs[3]~feeder_combout\ : std_logic;
SIGNAL \REGA1|dffs[0]~feeder_combout\ : std_logic;
SIGNAL \S1[7]~input_o\ : std_logic;
SIGNAL \S1[1]~input_o\ : std_logic;
SIGNAL \Mux38~0_combout\ : std_logic;
SIGNAL \S1[3]~input_o\ : std_logic;
SIGNAL \Mux36~0_combout\ : std_logic;
SIGNAL \S1[5]~input_o\ : std_logic;
SIGNAL \Mux34~0_combout\ : std_logic;
SIGNAL \Mux32~0_combout\ : std_logic;
SIGNAL \DA1[19]~1_combout\ : std_logic;
SIGNAL \REGA1|dffs[16]~feeder_combout\ : std_logic;
SIGNAL \REGA1|dffs[13]~feeder_combout\ : std_logic;
SIGNAL \REGA1|dffs[10]~feeder_combout\ : std_logic;
SIGNAL \REGA1|dffs[7]~feeder_combout\ : std_logic;
SIGNAL \REGA1|dffs[4]~feeder_combout\ : std_logic;
SIGNAL \REGA1|dffs[1]~feeder_combout\ : std_logic;
SIGNAL \REGA1|dffs[20]~0_combout\ : std_logic;
SIGNAL \REGA1|dffs[17]~feeder_combout\ : std_logic;
SIGNAL \REGA1|dffs[14]~feeder_combout\ : std_logic;
SIGNAL \REGA1|dffs[11]~feeder_combout\ : std_logic;
SIGNAL \REGA1|dffs[8]~feeder_combout\ : std_logic;
SIGNAL \REGA1|dffs[5]~feeder_combout\ : std_logic;
SIGNAL \REGA1|dffs[2]~feeder_combout\ : std_logic;
SIGNAL \S2[4]~input_o\ : std_logic;
SIGNAL \S2[2]~input_o\ : std_logic;
SIGNAL \S2[0]~input_o\ : std_logic;
SIGNAL \Mux47~0_combout\ : std_logic;
SIGNAL \REGS2|dffs[0]~0_combout\ : std_logic;
SIGNAL \Mux45~0_combout\ : std_logic;
SIGNAL \Mux43~0_combout\ : std_logic;
SIGNAL \S2[6]~input_o\ : std_logic;
SIGNAL \Mux41~0_combout\ : std_logic;
SIGNAL \DA2[18]~0_combout\ : std_logic;
SIGNAL \REGA2|dffs[3]~feeder_combout\ : std_logic;
SIGNAL \REGA2|dffs[3]~DUPLICATE_q\ : std_logic;
SIGNAL \REGA2|dffs[0]~feeder_combout\ : std_logic;
SIGNAL \S2[5]~input_o\ : std_logic;
SIGNAL \S2[1]~input_o\ : std_logic;
SIGNAL \Mux46~0_combout\ : std_logic;
SIGNAL \S2[3]~input_o\ : std_logic;
SIGNAL \Mux44~0_combout\ : std_logic;
SIGNAL \Mux42~0_combout\ : std_logic;
SIGNAL \S2[7]~input_o\ : std_logic;
SIGNAL \Mux40~0_combout\ : std_logic;
SIGNAL \DA2[19]~1_combout\ : std_logic;
SIGNAL \REGA2|dffs[16]~feeder_combout\ : std_logic;
SIGNAL \REGA2|dffs[13]~feeder_combout\ : std_logic;
SIGNAL \REGA2|dffs[10]~feeder_combout\ : std_logic;
SIGNAL \REGA2|dffs[7]~feeder_combout\ : std_logic;
SIGNAL \REGA2|dffs[4]~feeder_combout\ : std_logic;
SIGNAL \REGA2|dffs[1]~feeder_combout\ : std_logic;
SIGNAL \REGA2|dffs[20]~0_combout\ : std_logic;
SIGNAL \REGA2|dffs[14]~feeder_combout\ : std_logic;
SIGNAL \REGA2|dffs[11]~feeder_combout\ : std_logic;
SIGNAL \REGA2|dffs[8]~feeder_combout\ : std_logic;
SIGNAL \REGA2|dffs[5]~feeder_combout\ : std_logic;
SIGNAL \REGA2|dffs[2]~feeder_combout\ : std_logic;
SIGNAL \REGA2|dffs[14]~DUPLICATE_q\ : std_logic;
SIGNAL \REGA2|dffs\ : std_logic_vector(20 DOWNTO 0);
SIGNAL \REGDIR|dffs\ : std_logic_vector(31 DOWNTO 0);
SIGNAL \REGA1|dffs\ : std_logic_vector(20 DOWNTO 0);
SIGNAL \REGS1|dffs\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \REGS2|dffs\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \REGA2|ALT_INV_dffs[3]~DUPLICATE_q\ : std_logic;
SIGNAL \ALT_INV_TB[31]~input_o\ : std_logic;
SIGNAL \ALT_INV_TB[30]~input_o\ : std_logic;
SIGNAL \ALT_INV_TB[29]~input_o\ : std_logic;
SIGNAL \ALT_INV_TB[28]~input_o\ : std_logic;
SIGNAL \ALT_INV_TB[27]~input_o\ : std_logic;
SIGNAL \ALT_INV_TB[26]~input_o\ : std_logic;
SIGNAL \ALT_INV_TB[25]~input_o\ : std_logic;
SIGNAL \ALT_INV_TB[24]~input_o\ : std_logic;
SIGNAL \ALT_INV_TB[23]~input_o\ : std_logic;
SIGNAL \ALT_INV_TB[22]~input_o\ : std_logic;
SIGNAL \ALT_INV_TB[21]~input_o\ : std_logic;
SIGNAL \ALT_INV_TB[20]~input_o\ : std_logic;
SIGNAL \ALT_INV_TB[19]~input_o\ : std_logic;
SIGNAL \ALT_INV_TB[18]~input_o\ : std_logic;
SIGNAL \ALT_INV_TB[17]~input_o\ : std_logic;
SIGNAL \ALT_INV_TB[16]~input_o\ : std_logic;
SIGNAL \ALT_INV_TB[15]~input_o\ : std_logic;
SIGNAL \ALT_INV_TB[14]~input_o\ : std_logic;
SIGNAL \ALT_INV_TB[13]~input_o\ : std_logic;
SIGNAL \ALT_INV_TB[12]~input_o\ : std_logic;
SIGNAL \ALT_INV_TB[11]~input_o\ : std_logic;
SIGNAL \ALT_INV_TB[10]~input_o\ : std_logic;
SIGNAL \ALT_INV_TB[9]~input_o\ : std_logic;
SIGNAL \ALT_INV_TB[8]~input_o\ : std_logic;
SIGNAL \ALT_INV_TB[7]~input_o\ : std_logic;
SIGNAL \ALT_INV_TB[6]~input_o\ : std_logic;
SIGNAL \ALT_INV_TB[5]~input_o\ : std_logic;
SIGNAL \ALT_INV_TB[4]~input_o\ : std_logic;
SIGNAL \ALT_INV_TB[3]~input_o\ : std_logic;
SIGNAL \ALT_INV_TB[2]~input_o\ : std_logic;
SIGNAL \ALT_INV_TB[1]~input_o\ : std_logic;
SIGNAL \ALT_INV_TB[0]~input_o\ : std_logic;
SIGNAL \ALT_INV_S2[7]~input_o\ : std_logic;
SIGNAL \ALT_INV_S2[6]~input_o\ : std_logic;
SIGNAL \ALT_INV_S2[5]~input_o\ : std_logic;
SIGNAL \ALT_INV_S2[4]~input_o\ : std_logic;
SIGNAL \ALT_INV_S2[3]~input_o\ : std_logic;
SIGNAL \ALT_INV_S2[2]~input_o\ : std_logic;
SIGNAL \ALT_INV_S2[1]~input_o\ : std_logic;
SIGNAL \ALT_INV_S2[0]~input_o\ : std_logic;
SIGNAL \ALT_INV_S1[7]~input_o\ : std_logic;
SIGNAL \ALT_INV_S1[6]~input_o\ : std_logic;
SIGNAL \ALT_INV_S1[5]~input_o\ : std_logic;
SIGNAL \ALT_INV_S1[4]~input_o\ : std_logic;
SIGNAL \ALT_INV_S1[3]~input_o\ : std_logic;
SIGNAL \ALT_INV_S1[2]~input_o\ : std_logic;
SIGNAL \ALT_INV_S1[1]~input_o\ : std_logic;
SIGNAL \ALT_INV_CLK~input_o\ : std_logic;
SIGNAL \ALT_INV_S1[0]~input_o\ : std_logic;
SIGNAL \REGDIR|ALT_INV_dffs\ : std_logic_vector(31 DOWNTO 0);
SIGNAL \REGS2|ALT_INV_dffs\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \REGS1|ALT_INV_dffs\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \REGA2|ALT_INV_dffs\ : std_logic_vector(19 DOWNTO 4);
SIGNAL \REGA1|ALT_INV_dffs\ : std_logic_vector(20 DOWNTO 3);

BEGIN

ww_CLK <= CLK;
ww_TB <= TB;
ww_S1 <= S1;
ww_S2 <= S2;
A1 <= ww_A1;
A2 <= ww_A2;
OS1 <= ww_OS1;
OS2 <= ww_OS2;
ODIR <= ww_ODIR;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
\REGA2|ALT_INV_dffs[3]~DUPLICATE_q\ <= NOT \REGA2|dffs[3]~DUPLICATE_q\;
\ALT_INV_TB[31]~input_o\ <= NOT \TB[31]~input_o\;
\ALT_INV_TB[30]~input_o\ <= NOT \TB[30]~input_o\;
\ALT_INV_TB[29]~input_o\ <= NOT \TB[29]~input_o\;
\ALT_INV_TB[28]~input_o\ <= NOT \TB[28]~input_o\;
\ALT_INV_TB[27]~input_o\ <= NOT \TB[27]~input_o\;
\ALT_INV_TB[26]~input_o\ <= NOT \TB[26]~input_o\;
\ALT_INV_TB[25]~input_o\ <= NOT \TB[25]~input_o\;
\ALT_INV_TB[24]~input_o\ <= NOT \TB[24]~input_o\;
\ALT_INV_TB[23]~input_o\ <= NOT \TB[23]~input_o\;
\ALT_INV_TB[22]~input_o\ <= NOT \TB[22]~input_o\;
\ALT_INV_TB[21]~input_o\ <= NOT \TB[21]~input_o\;
\ALT_INV_TB[20]~input_o\ <= NOT \TB[20]~input_o\;
\ALT_INV_TB[19]~input_o\ <= NOT \TB[19]~input_o\;
\ALT_INV_TB[18]~input_o\ <= NOT \TB[18]~input_o\;
\ALT_INV_TB[17]~input_o\ <= NOT \TB[17]~input_o\;
\ALT_INV_TB[16]~input_o\ <= NOT \TB[16]~input_o\;
\ALT_INV_TB[15]~input_o\ <= NOT \TB[15]~input_o\;
\ALT_INV_TB[14]~input_o\ <= NOT \TB[14]~input_o\;
\ALT_INV_TB[13]~input_o\ <= NOT \TB[13]~input_o\;
\ALT_INV_TB[12]~input_o\ <= NOT \TB[12]~input_o\;
\ALT_INV_TB[11]~input_o\ <= NOT \TB[11]~input_o\;
\ALT_INV_TB[10]~input_o\ <= NOT \TB[10]~input_o\;
\ALT_INV_TB[9]~input_o\ <= NOT \TB[9]~input_o\;
\ALT_INV_TB[8]~input_o\ <= NOT \TB[8]~input_o\;
\ALT_INV_TB[7]~input_o\ <= NOT \TB[7]~input_o\;
\ALT_INV_TB[6]~input_o\ <= NOT \TB[6]~input_o\;
\ALT_INV_TB[5]~input_o\ <= NOT \TB[5]~input_o\;
\ALT_INV_TB[4]~input_o\ <= NOT \TB[4]~input_o\;
\ALT_INV_TB[3]~input_o\ <= NOT \TB[3]~input_o\;
\ALT_INV_TB[2]~input_o\ <= NOT \TB[2]~input_o\;
\ALT_INV_TB[1]~input_o\ <= NOT \TB[1]~input_o\;
\ALT_INV_TB[0]~input_o\ <= NOT \TB[0]~input_o\;
\ALT_INV_S2[7]~input_o\ <= NOT \S2[7]~input_o\;
\ALT_INV_S2[6]~input_o\ <= NOT \S2[6]~input_o\;
\ALT_INV_S2[5]~input_o\ <= NOT \S2[5]~input_o\;
\ALT_INV_S2[4]~input_o\ <= NOT \S2[4]~input_o\;
\ALT_INV_S2[3]~input_o\ <= NOT \S2[3]~input_o\;
\ALT_INV_S2[2]~input_o\ <= NOT \S2[2]~input_o\;
\ALT_INV_S2[1]~input_o\ <= NOT \S2[1]~input_o\;
\ALT_INV_S2[0]~input_o\ <= NOT \S2[0]~input_o\;
\ALT_INV_S1[7]~input_o\ <= NOT \S1[7]~input_o\;
\ALT_INV_S1[6]~input_o\ <= NOT \S1[6]~input_o\;
\ALT_INV_S1[5]~input_o\ <= NOT \S1[5]~input_o\;
\ALT_INV_S1[4]~input_o\ <= NOT \S1[4]~input_o\;
\ALT_INV_S1[3]~input_o\ <= NOT \S1[3]~input_o\;
\ALT_INV_S1[2]~input_o\ <= NOT \S1[2]~input_o\;
\ALT_INV_S1[1]~input_o\ <= NOT \S1[1]~input_o\;
\ALT_INV_CLK~input_o\ <= NOT \CLK~input_o\;
\ALT_INV_S1[0]~input_o\ <= NOT \S1[0]~input_o\;
\REGDIR|ALT_INV_dffs\(31) <= NOT \REGDIR|dffs\(31);
\REGDIR|ALT_INV_dffs\(30) <= NOT \REGDIR|dffs\(30);
\REGDIR|ALT_INV_dffs\(29) <= NOT \REGDIR|dffs\(29);
\REGDIR|ALT_INV_dffs\(28) <= NOT \REGDIR|dffs\(28);
\REGDIR|ALT_INV_dffs\(27) <= NOT \REGDIR|dffs\(27);
\REGDIR|ALT_INV_dffs\(26) <= NOT \REGDIR|dffs\(26);
\REGDIR|ALT_INV_dffs\(25) <= NOT \REGDIR|dffs\(25);
\REGDIR|ALT_INV_dffs\(24) <= NOT \REGDIR|dffs\(24);
\REGDIR|ALT_INV_dffs\(23) <= NOT \REGDIR|dffs\(23);
\REGDIR|ALT_INV_dffs\(22) <= NOT \REGDIR|dffs\(22);
\REGDIR|ALT_INV_dffs\(21) <= NOT \REGDIR|dffs\(21);
\REGDIR|ALT_INV_dffs\(20) <= NOT \REGDIR|dffs\(20);
\REGDIR|ALT_INV_dffs\(19) <= NOT \REGDIR|dffs\(19);
\REGDIR|ALT_INV_dffs\(18) <= NOT \REGDIR|dffs\(18);
\REGDIR|ALT_INV_dffs\(17) <= NOT \REGDIR|dffs\(17);
\REGDIR|ALT_INV_dffs\(16) <= NOT \REGDIR|dffs\(16);
\REGDIR|ALT_INV_dffs\(15) <= NOT \REGDIR|dffs\(15);
\REGDIR|ALT_INV_dffs\(14) <= NOT \REGDIR|dffs\(14);
\REGDIR|ALT_INV_dffs\(13) <= NOT \REGDIR|dffs\(13);
\REGDIR|ALT_INV_dffs\(12) <= NOT \REGDIR|dffs\(12);
\REGDIR|ALT_INV_dffs\(11) <= NOT \REGDIR|dffs\(11);
\REGDIR|ALT_INV_dffs\(10) <= NOT \REGDIR|dffs\(10);
\REGDIR|ALT_INV_dffs\(9) <= NOT \REGDIR|dffs\(9);
\REGDIR|ALT_INV_dffs\(8) <= NOT \REGDIR|dffs\(8);
\REGDIR|ALT_INV_dffs\(1) <= NOT \REGDIR|dffs\(1);
\REGDIR|ALT_INV_dffs\(0) <= NOT \REGDIR|dffs\(0);
\REGS2|ALT_INV_dffs\(7) <= NOT \REGS2|dffs\(7);
\REGS2|ALT_INV_dffs\(6) <= NOT \REGS2|dffs\(6);
\REGS2|ALT_INV_dffs\(5) <= NOT \REGS2|dffs\(5);
\REGS2|ALT_INV_dffs\(4) <= NOT \REGS2|dffs\(4);
\REGS2|ALT_INV_dffs\(3) <= NOT \REGS2|dffs\(3);
\REGS2|ALT_INV_dffs\(2) <= NOT \REGS2|dffs\(2);
\REGS2|ALT_INV_dffs\(1) <= NOT \REGS2|dffs\(1);
\REGS2|ALT_INV_dffs\(0) <= NOT \REGS2|dffs\(0);
\REGS1|ALT_INV_dffs\(7) <= NOT \REGS1|dffs\(7);
\REGS1|ALT_INV_dffs\(6) <= NOT \REGS1|dffs\(6);
\REGS1|ALT_INV_dffs\(5) <= NOT \REGS1|dffs\(5);
\REGS1|ALT_INV_dffs\(4) <= NOT \REGS1|dffs\(4);
\REGS1|ALT_INV_dffs\(3) <= NOT \REGS1|dffs\(3);
\REGS1|ALT_INV_dffs\(2) <= NOT \REGS1|dffs\(2);
\REGS1|ALT_INV_dffs\(1) <= NOT \REGS1|dffs\(1);
\REGS1|ALT_INV_dffs\(0) <= NOT \REGS1|dffs\(0);
\REGA2|ALT_INV_dffs\(19) <= NOT \REGA2|dffs\(19);
\REGA2|ALT_INV_dffs\(17) <= NOT \REGA2|dffs\(17);
\REGA2|ALT_INV_dffs\(16) <= NOT \REGA2|dffs\(16);
\REGA2|ALT_INV_dffs\(14) <= NOT \REGA2|dffs\(14);
\REGA2|ALT_INV_dffs\(13) <= NOT \REGA2|dffs\(13);
\REGA2|ALT_INV_dffs\(11) <= NOT \REGA2|dffs\(11);
\REGA2|ALT_INV_dffs\(10) <= NOT \REGA2|dffs\(10);
\REGA2|ALT_INV_dffs\(8) <= NOT \REGA2|dffs\(8);
\REGA2|ALT_INV_dffs\(7) <= NOT \REGA2|dffs\(7);
\REGA2|ALT_INV_dffs\(6) <= NOT \REGA2|dffs\(6);
\REGA2|ALT_INV_dffs\(5) <= NOT \REGA2|dffs\(5);
\REGA2|ALT_INV_dffs\(4) <= NOT \REGA2|dffs\(4);
\REGA1|ALT_INV_dffs\(20) <= NOT \REGA1|dffs\(20);
\REGA1|ALT_INV_dffs\(19) <= NOT \REGA1|dffs\(19);
\REGA1|ALT_INV_dffs\(17) <= NOT \REGA1|dffs\(17);
\REGA1|ALT_INV_dffs\(16) <= NOT \REGA1|dffs\(16);
\REGA1|ALT_INV_dffs\(15) <= NOT \REGA1|dffs\(15);
\REGA1|ALT_INV_dffs\(14) <= NOT \REGA1|dffs\(14);
\REGA1|ALT_INV_dffs\(13) <= NOT \REGA1|dffs\(13);
\REGA1|ALT_INV_dffs\(11) <= NOT \REGA1|dffs\(11);
\REGA1|ALT_INV_dffs\(10) <= NOT \REGA1|dffs\(10);
\REGA1|ALT_INV_dffs\(8) <= NOT \REGA1|dffs\(8);
\REGA1|ALT_INV_dffs\(7) <= NOT \REGA1|dffs\(7);
\REGA1|ALT_INV_dffs\(6) <= NOT \REGA1|dffs\(6);
\REGA1|ALT_INV_dffs\(5) <= NOT \REGA1|dffs\(5);
\REGA1|ALT_INV_dffs\(4) <= NOT \REGA1|dffs\(4);
\REGA1|ALT_INV_dffs\(3) <= NOT \REGA1|dffs\(3);
\REGDIR|ALT_INV_dffs\(7) <= NOT \REGDIR|dffs\(7);
\REGDIR|ALT_INV_dffs\(6) <= NOT \REGDIR|dffs\(6);
\REGDIR|ALT_INV_dffs\(5) <= NOT \REGDIR|dffs\(5);
\REGDIR|ALT_INV_dffs\(4) <= NOT \REGDIR|dffs\(4);
\REGDIR|ALT_INV_dffs\(3) <= NOT \REGDIR|dffs\(3);
\REGDIR|ALT_INV_dffs\(2) <= NOT \REGDIR|dffs\(2);

-- Location: IOOBUF_X32_Y0_N19
\A1[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGA1|dffs\(0),
	devoe => ww_devoe,
	o => ww_A1(0));

-- Location: IOOBUF_X54_Y0_N53
\A1[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGA1|dffs\(1),
	devoe => ww_devoe,
	o => ww_A1(1));

-- Location: IOOBUF_X52_Y0_N2
\A1[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGA1|dffs\(2),
	devoe => ww_devoe,
	o => ww_A1(2));

-- Location: IOOBUF_X58_Y0_N93
\A1[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGA1|dffs\(3),
	devoe => ww_devoe,
	o => ww_A1(3));

-- Location: IOOBUF_X70_Y81_N19
\A1[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGA1|dffs\(4),
	devoe => ww_devoe,
	o => ww_A1(4));

-- Location: IOOBUF_X52_Y0_N36
\A1[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGA1|dffs\(5),
	devoe => ww_devoe,
	o => ww_A1(5));

-- Location: IOOBUF_X28_Y0_N19
\A1[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGA1|dffs\(6),
	devoe => ww_devoe,
	o => ww_A1(6));

-- Location: IOOBUF_X52_Y0_N53
\A1[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGA1|dffs\(7),
	devoe => ww_devoe,
	o => ww_A1(7));

-- Location: IOOBUF_X54_Y0_N36
\A1[8]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGA1|dffs\(8),
	devoe => ww_devoe,
	o => ww_A1(8));

-- Location: IOOBUF_X60_Y0_N36
\A1[9]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGA1|dffs\(9),
	devoe => ww_devoe,
	o => ww_A1(9));

-- Location: IOOBUF_X54_Y0_N19
\A1[10]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGA1|dffs\(10),
	devoe => ww_devoe,
	o => ww_A1(10));

-- Location: IOOBUF_X36_Y0_N36
\A1[11]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGA1|dffs\(11),
	devoe => ww_devoe,
	o => ww_A1(11));

-- Location: IOOBUF_X66_Y81_N42
\A1[12]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGA1|dffs\(12),
	devoe => ww_devoe,
	o => ww_A1(12));

-- Location: IOOBUF_X52_Y0_N19
\A1[13]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGA1|dffs\(13),
	devoe => ww_devoe,
	o => ww_A1(13));

-- Location: IOOBUF_X62_Y0_N36
\A1[14]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGA1|dffs\(14),
	devoe => ww_devoe,
	o => ww_A1(14));

-- Location: IOOBUF_X89_Y8_N5
\A1[15]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGA1|dffs\(15),
	devoe => ww_devoe,
	o => ww_A1(15));

-- Location: IOOBUF_X58_Y0_N76
\A1[16]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGA1|dffs\(16),
	devoe => ww_devoe,
	o => ww_A1(16));

-- Location: IOOBUF_X4_Y0_N2
\A1[17]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGA1|dffs\(17),
	devoe => ww_devoe,
	o => ww_A1(17));

-- Location: IOOBUF_X2_Y0_N93
\A1[18]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGA1|dffs\(18),
	devoe => ww_devoe,
	o => ww_A1(18));

-- Location: IOOBUF_X64_Y81_N2
\A1[19]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGA1|dffs\(19),
	devoe => ww_devoe,
	o => ww_A1(19));

-- Location: IOOBUF_X66_Y81_N76
\A1[20]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGA1|dffs\(20),
	devoe => ww_devoe,
	o => ww_A1(20));

-- Location: IOOBUF_X89_Y38_N22
\A2[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGA2|dffs\(0),
	devoe => ww_devoe,
	o => ww_A2(0));

-- Location: IOOBUF_X38_Y0_N36
\A2[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGA2|dffs\(1),
	devoe => ww_devoe,
	o => ww_A2(1));

-- Location: IOOBUF_X54_Y0_N2
\A2[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGA2|dffs\(2),
	devoe => ww_devoe,
	o => ww_A2(2));

-- Location: IOOBUF_X66_Y81_N93
\A2[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGA2|dffs\(3),
	devoe => ww_devoe,
	o => ww_A2(3));

-- Location: IOOBUF_X58_Y0_N59
\A2[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGA2|dffs\(4),
	devoe => ww_devoe,
	o => ww_A2(4));

-- Location: IOOBUF_X60_Y81_N36
\A2[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGA2|dffs\(5),
	devoe => ww_devoe,
	o => ww_A2(5));

-- Location: IOOBUF_X6_Y0_N19
\A2[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGA2|dffs\(6),
	devoe => ww_devoe,
	o => ww_A2(6));

-- Location: IOOBUF_X30_Y0_N2
\A2[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGA2|dffs\(7),
	devoe => ww_devoe,
	o => ww_A2(7));

-- Location: IOOBUF_X2_Y0_N42
\A2[8]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGA2|dffs\(8),
	devoe => ww_devoe,
	o => ww_A2(8));

-- Location: IOOBUF_X4_Y0_N53
\A2[9]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGA2|dffs\(9),
	devoe => ww_devoe,
	o => ww_A2(9));

-- Location: IOOBUF_X6_Y0_N2
\A2[10]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGA2|dffs\(10),
	devoe => ww_devoe,
	o => ww_A2(10));

-- Location: IOOBUF_X30_Y0_N53
\A2[11]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGA2|dffs\(11),
	devoe => ww_devoe,
	o => ww_A2(11));

-- Location: IOOBUF_X28_Y0_N36
\A2[12]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGA2|dffs\(12),
	devoe => ww_devoe,
	o => ww_A2(12));

-- Location: IOOBUF_X58_Y0_N42
\A2[13]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGA2|dffs\(13),
	devoe => ww_devoe,
	o => ww_A2(13));

-- Location: IOOBUF_X56_Y0_N19
\A2[14]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGA2|dffs[14]~DUPLICATE_q\,
	devoe => ww_devoe,
	o => ww_A2(14));

-- Location: IOOBUF_X66_Y81_N59
\A2[15]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGA2|dffs\(15),
	devoe => ww_devoe,
	o => ww_A2(15));

-- Location: IOOBUF_X30_Y0_N19
\A2[16]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGA2|dffs\(16),
	devoe => ww_devoe,
	o => ww_A2(16));

-- Location: IOOBUF_X89_Y4_N62
\A2[17]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGA2|dffs\(17),
	devoe => ww_devoe,
	o => ww_A2(17));

-- Location: IOOBUF_X28_Y0_N2
\A2[18]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGA2|dffs\(18),
	devoe => ww_devoe,
	o => ww_A2(18));

-- Location: IOOBUF_X60_Y0_N53
\A2[19]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGA2|dffs\(19),
	devoe => ww_devoe,
	o => ww_A2(19));

-- Location: IOOBUF_X89_Y37_N39
\A2[20]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGA2|dffs\(20),
	devoe => ww_devoe,
	o => ww_A2(20));

-- Location: IOOBUF_X38_Y0_N2
\OS1[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGS1|dffs\(0),
	devoe => ww_devoe,
	o => ww_OS1(0));

-- Location: IOOBUF_X89_Y37_N22
\OS1[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGS1|dffs\(1),
	devoe => ww_devoe,
	o => ww_OS1(1));

-- Location: IOOBUF_X89_Y6_N56
\OS1[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGS1|dffs\(2),
	devoe => ww_devoe,
	o => ww_OS1(2));

-- Location: IOOBUF_X89_Y9_N56
\OS1[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGS1|dffs\(3),
	devoe => ww_devoe,
	o => ww_OS1(3));

-- Location: IOOBUF_X89_Y36_N56
\OS1[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGS1|dffs\(4),
	devoe => ww_devoe,
	o => ww_OS1(4));

-- Location: IOOBUF_X40_Y0_N53
\OS1[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGS1|dffs\(5),
	devoe => ww_devoe,
	o => ww_OS1(5));

-- Location: IOOBUF_X89_Y4_N79
\OS1[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGS1|dffs\(6),
	devoe => ww_devoe,
	o => ww_OS1(6));

-- Location: IOOBUF_X89_Y4_N96
\OS1[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGS1|dffs\(7),
	devoe => ww_devoe,
	o => ww_OS1(7));

-- Location: IOOBUF_X26_Y0_N76
\OS2[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGS2|dffs\(0),
	devoe => ww_devoe,
	o => ww_OS2(0));

-- Location: IOOBUF_X89_Y8_N22
\OS2[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGS2|dffs\(1),
	devoe => ww_devoe,
	o => ww_OS2(1));

-- Location: IOOBUF_X89_Y9_N5
\OS2[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGS2|dffs\(2),
	devoe => ww_devoe,
	o => ww_OS2(2));

-- Location: IOOBUF_X89_Y35_N45
\OS2[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGS2|dffs\(3),
	devoe => ww_devoe,
	o => ww_OS2(3));

-- Location: IOOBUF_X89_Y37_N56
\OS2[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGS2|dffs\(4),
	devoe => ww_devoe,
	o => ww_OS2(4));

-- Location: IOOBUF_X26_Y0_N59
\OS2[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGS2|dffs\(5),
	devoe => ww_devoe,
	o => ww_OS2(5));

-- Location: IOOBUF_X64_Y0_N2
\OS2[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGS2|dffs\(6),
	devoe => ww_devoe,
	o => ww_OS2(6));

-- Location: IOOBUF_X89_Y4_N45
\OS2[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGS2|dffs\(7),
	devoe => ww_devoe,
	o => ww_OS2(7));

-- Location: IOOBUF_X68_Y0_N36
\ODIR[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGDIR|dffs\(0),
	devoe => ww_devoe,
	o => ww_ODIR(0));

-- Location: IOOBUF_X64_Y0_N36
\ODIR[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGDIR|dffs\(1),
	devoe => ww_devoe,
	o => ww_ODIR(1));

-- Location: IOOBUF_X89_Y6_N39
\ODIR[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGDIR|dffs\(2),
	devoe => ww_devoe,
	o => ww_ODIR(2));

-- Location: IOOBUF_X60_Y0_N19
\ODIR[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGDIR|dffs\(3),
	devoe => ww_devoe,
	o => ww_ODIR(3));

-- Location: IOOBUF_X72_Y0_N36
\ODIR[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGDIR|dffs\(4),
	devoe => ww_devoe,
	o => ww_ODIR(4));

-- Location: IOOBUF_X32_Y0_N2
\ODIR[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGDIR|dffs\(5),
	devoe => ww_devoe,
	o => ww_ODIR(5));

-- Location: IOOBUF_X89_Y8_N56
\ODIR[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGDIR|dffs\(6),
	devoe => ww_devoe,
	o => ww_ODIR(6));

-- Location: IOOBUF_X4_Y0_N19
\ODIR[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGDIR|dffs\(7),
	devoe => ww_devoe,
	o => ww_ODIR(7));

-- Location: IOOBUF_X68_Y0_N19
\ODIR[8]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGDIR|dffs\(8),
	devoe => ww_devoe,
	o => ww_ODIR(8));

-- Location: IOOBUF_X28_Y0_N53
\ODIR[9]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGDIR|dffs\(9),
	devoe => ww_devoe,
	o => ww_ODIR(9));

-- Location: IOOBUF_X70_Y0_N19
\ODIR[10]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGDIR|dffs\(10),
	devoe => ww_devoe,
	o => ww_ODIR(10));

-- Location: IOOBUF_X38_Y0_N53
\ODIR[11]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGDIR|dffs\(11),
	devoe => ww_devoe,
	o => ww_ODIR(11));

-- Location: IOOBUF_X68_Y0_N53
\ODIR[12]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGDIR|dffs\(12),
	devoe => ww_devoe,
	o => ww_ODIR(12));

-- Location: IOOBUF_X60_Y0_N2
\ODIR[13]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGDIR|dffs\(13),
	devoe => ww_devoe,
	o => ww_ODIR(13));

-- Location: IOOBUF_X72_Y0_N19
\ODIR[14]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGDIR|dffs\(14),
	devoe => ww_devoe,
	o => ww_ODIR(14));

-- Location: IOOBUF_X34_Y0_N93
\ODIR[15]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGDIR|dffs\(15),
	devoe => ww_devoe,
	o => ww_ODIR(15));

-- Location: IOOBUF_X72_Y0_N2
\ODIR[16]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGDIR|dffs\(16),
	devoe => ww_devoe,
	o => ww_ODIR(16));

-- Location: IOOBUF_X66_Y0_N42
\ODIR[17]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGDIR|dffs\(17),
	devoe => ww_devoe,
	o => ww_ODIR(17));

-- Location: IOOBUF_X70_Y0_N53
\ODIR[18]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGDIR|dffs\(18),
	devoe => ww_devoe,
	o => ww_ODIR(18));

-- Location: IOOBUF_X64_Y0_N53
\ODIR[19]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGDIR|dffs\(19),
	devoe => ww_devoe,
	o => ww_ODIR(19));

-- Location: IOOBUF_X89_Y9_N39
\ODIR[20]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGDIR|dffs\(20),
	devoe => ww_devoe,
	o => ww_ODIR(20));

-- Location: IOOBUF_X8_Y0_N36
\ODIR[21]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGDIR|dffs\(21),
	devoe => ww_devoe,
	o => ww_ODIR(21));

-- Location: IOOBUF_X72_Y0_N53
\ODIR[22]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGDIR|dffs\(22),
	devoe => ww_devoe,
	o => ww_ODIR(22));

-- Location: IOOBUF_X64_Y0_N19
\ODIR[23]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGDIR|dffs\(23),
	devoe => ww_devoe,
	o => ww_ODIR(23));

-- Location: IOOBUF_X89_Y37_N5
\ODIR[24]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGDIR|dffs\(24),
	devoe => ww_devoe,
	o => ww_ODIR(24));

-- Location: IOOBUF_X66_Y0_N93
\ODIR[25]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGDIR|dffs\(25),
	devoe => ww_devoe,
	o => ww_ODIR(25));

-- Location: IOOBUF_X89_Y36_N5
\ODIR[26]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGDIR|dffs\(26),
	devoe => ww_devoe,
	o => ww_ODIR(26));

-- Location: IOOBUF_X50_Y0_N59
\ODIR[27]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGDIR|dffs\(27),
	devoe => ww_devoe,
	o => ww_ODIR(27));

-- Location: IOOBUF_X70_Y0_N36
\ODIR[28]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGDIR|dffs\(28),
	devoe => ww_devoe,
	o => ww_ODIR(28));

-- Location: IOOBUF_X68_Y0_N2
\ODIR[29]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGDIR|dffs\(29),
	devoe => ww_devoe,
	o => ww_ODIR(29));

-- Location: IOOBUF_X66_Y0_N76
\ODIR[30]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGDIR|dffs\(30),
	devoe => ww_devoe,
	o => ww_ODIR(30));

-- Location: IOOBUF_X66_Y0_N59
\ODIR[31]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \REGDIR|dffs\(31),
	devoe => ww_devoe,
	o => ww_ODIR(31));

-- Location: IOIBUF_X89_Y35_N61
\CLK~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CLK,
	o => \CLK~input_o\);

-- Location: CLKCTRL_G10
\CLK~inputCLKENA0\ : cyclonev_clkena
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	disable_mode => "low",
	ena_register_mode => "always enabled",
	ena_register_power_up => "high",
	test_syn => "high")
-- pragma translate_on
PORT MAP (
	inclk => \CLK~input_o\,
	outclk => \CLK~inputCLKENA0_outclk\);

-- Location: IOIBUF_X38_Y0_N18
\TB[7]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_TB(7),
	o => \TB[7]~input_o\);

-- Location: IOIBUF_X36_Y0_N18
\TB[5]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_TB(5),
	o => \TB[5]~input_o\);

-- Location: IOIBUF_X56_Y0_N52
\TB[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_TB(1),
	o => \TB[1]~input_o\);

-- Location: IOIBUF_X6_Y0_N35
\TB[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_TB(0),
	o => \TB[0]~input_o\);

-- Location: LABCELL_X67_Y3_N33
\Mux31~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux31~0_combout\ = (!\REGDIR|dffs\(31) & (\TB[0]~input_o\ & !\REGDIR|dffs\(30)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110000000000000011000000000000001100000000000000110000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \REGDIR|ALT_INV_dffs\(31),
	datac => \ALT_INV_TB[0]~input_o\,
	datad => \REGDIR|ALT_INV_dffs\(30),
	combout => \Mux31~0_combout\);

-- Location: FF_X67_Y3_N35
\REGDIR|dffs[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux31~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGDIR|dffs\(0));

-- Location: IOIBUF_X50_Y0_N41
\TB[10]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_TB(10),
	o => \TB[10]~input_o\);

-- Location: IOIBUF_X89_Y6_N4
\TB[6]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_TB(6),
	o => \TB[6]~input_o\);

-- Location: IOIBUF_X62_Y0_N18
\TB[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_TB(2),
	o => \TB[2]~input_o\);

-- Location: LABCELL_X67_Y3_N12
\Mux29~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux29~0_combout\ = ( \REGDIR|dffs\(31) & ( \REGDIR|dffs\(0) ) ) # ( !\REGDIR|dffs\(31) & ( \TB[2]~input_o\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \REGDIR|ALT_INV_dffs\(0),
	datad => \ALT_INV_TB[2]~input_o\,
	dataf => \REGDIR|ALT_INV_dffs\(31),
	combout => \Mux29~0_combout\);

-- Location: FF_X67_Y3_N14
\REGDIR|dffs[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux29~0_combout\,
	sclr => \REGDIR|dffs\(30),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGDIR|dffs\(2));

-- Location: IOIBUF_X62_Y0_N52
\TB[4]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_TB(4),
	o => \TB[4]~input_o\);

-- Location: LABCELL_X68_Y4_N54
\Mux27~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux27~0_combout\ = ( \REGDIR|dffs\(31) & ( \REGDIR|dffs\(2) ) ) # ( !\REGDIR|dffs\(31) & ( \TB[4]~input_o\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111010101010101010100001111000011110101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \REGDIR|ALT_INV_dffs\(2),
	datac => \ALT_INV_TB[4]~input_o\,
	datae => \REGDIR|ALT_INV_dffs\(31),
	combout => \Mux27~0_combout\);

-- Location: FF_X68_Y4_N56
\REGDIR|dffs[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux27~0_combout\,
	sclr => \REGDIR|dffs\(30),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGDIR|dffs\(4));

-- Location: LABCELL_X68_Y4_N39
\Mux25~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux25~0_combout\ = ( \REGDIR|dffs\(4) & ( (\TB[6]~input_o\) # (\REGDIR|dffs\(31)) ) ) # ( !\REGDIR|dffs\(4) & ( (!\REGDIR|dffs\(31) & \TB[6]~input_o\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000101000001010000010100000101001011111010111110101111101011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \REGDIR|ALT_INV_dffs\(31),
	datac => \ALT_INV_TB[6]~input_o\,
	dataf => \REGDIR|ALT_INV_dffs\(4),
	combout => \Mux25~0_combout\);

-- Location: FF_X68_Y4_N41
\REGDIR|dffs[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux25~0_combout\,
	sclr => \REGDIR|dffs\(30),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGDIR|dffs\(6));

-- Location: IOIBUF_X68_Y81_N52
\TB[8]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_TB(8),
	o => \TB[8]~input_o\);

-- Location: LABCELL_X68_Y4_N36
\Mux23~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux23~0_combout\ = ( \REGDIR|dffs\(0) & ( (!\REGDIR|dffs\(31) & (((\TB[8]~input_o\) # (\REGDIR|dffs\(30))))) # (\REGDIR|dffs\(31) & (\REGDIR|dffs\(6) & (!\REGDIR|dffs\(30)))) ) ) # ( !\REGDIR|dffs\(0) & ( (!\REGDIR|dffs\(30) & ((!\REGDIR|dffs\(31) & 
-- ((\TB[8]~input_o\))) # (\REGDIR|dffs\(31) & (\REGDIR|dffs\(6))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000010110000000100001011000000011010101110100001101010111010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \REGDIR|ALT_INV_dffs\(31),
	datab => \REGDIR|ALT_INV_dffs\(6),
	datac => \REGDIR|ALT_INV_dffs\(30),
	datad => \ALT_INV_TB[8]~input_o\,
	dataf => \REGDIR|ALT_INV_dffs\(0),
	combout => \Mux23~0_combout\);

-- Location: FF_X68_Y4_N38
\REGDIR|dffs[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux23~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGDIR|dffs\(8));

-- Location: LABCELL_X67_Y3_N36
\Mux21~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux21~0_combout\ = ( \REGDIR|dffs\(30) & ( \REGDIR|dffs\(2) & ( (!\REGDIR|dffs\(31)) # (\REGDIR|dffs\(0)) ) ) ) # ( !\REGDIR|dffs\(30) & ( \REGDIR|dffs\(2) & ( (!\REGDIR|dffs\(31) & (\TB[10]~input_o\)) # (\REGDIR|dffs\(31) & ((\REGDIR|dffs\(8)))) ) ) ) # 
-- ( \REGDIR|dffs\(30) & ( !\REGDIR|dffs\(2) & ( (\REGDIR|dffs\(0) & \REGDIR|dffs\(31)) ) ) ) # ( !\REGDIR|dffs\(30) & ( !\REGDIR|dffs\(2) & ( (!\REGDIR|dffs\(31) & (\TB[10]~input_o\)) # (\REGDIR|dffs\(31) & ((\REGDIR|dffs\(8)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100001111000000000101010100110011000011111111111101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \REGDIR|ALT_INV_dffs\(0),
	datab => \ALT_INV_TB[10]~input_o\,
	datac => \REGDIR|ALT_INV_dffs\(8),
	datad => \REGDIR|ALT_INV_dffs\(31),
	datae => \REGDIR|ALT_INV_dffs\(30),
	dataf => \REGDIR|ALT_INV_dffs\(2),
	combout => \Mux21~0_combout\);

-- Location: FF_X67_Y3_N38
\REGDIR|dffs[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux21~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGDIR|dffs\(10));

-- Location: IOIBUF_X68_Y81_N35
\TB[12]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_TB(12),
	o => \TB[12]~input_o\);

-- Location: LABCELL_X67_Y3_N54
\Mux19~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux19~0_combout\ = ( \REGDIR|dffs\(30) & ( \REGDIR|dffs\(2) & ( (\REGDIR|dffs\(31)) # (\REGDIR|dffs\(4)) ) ) ) # ( !\REGDIR|dffs\(30) & ( \REGDIR|dffs\(2) & ( (!\REGDIR|dffs\(31) & ((\TB[12]~input_o\))) # (\REGDIR|dffs\(31) & (\REGDIR|dffs\(10))) ) ) ) # 
-- ( \REGDIR|dffs\(30) & ( !\REGDIR|dffs\(2) & ( (\REGDIR|dffs\(4) & !\REGDIR|dffs\(31)) ) ) ) # ( !\REGDIR|dffs\(30) & ( !\REGDIR|dffs\(2) & ( (!\REGDIR|dffs\(31) & ((\TB[12]~input_o\))) # (\REGDIR|dffs\(31) & (\REGDIR|dffs\(10))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111101010101001100110000000000001111010101010011001111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \REGDIR|ALT_INV_dffs\(10),
	datab => \REGDIR|ALT_INV_dffs\(4),
	datac => \ALT_INV_TB[12]~input_o\,
	datad => \REGDIR|ALT_INV_dffs\(31),
	datae => \REGDIR|ALT_INV_dffs\(30),
	dataf => \REGDIR|ALT_INV_dffs\(2),
	combout => \Mux19~0_combout\);

-- Location: FF_X67_Y3_N56
\REGDIR|dffs[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux19~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGDIR|dffs\(12));

-- Location: IOIBUF_X50_Y0_N92
\TB[14]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_TB(14),
	o => \TB[14]~input_o\);

-- Location: LABCELL_X68_Y4_N51
\Mux17~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux17~0_combout\ = ( \REGDIR|dffs\(12) & ( \REGDIR|dffs\(4) & ( ((!\REGDIR|dffs\(30) & (\TB[14]~input_o\)) # (\REGDIR|dffs\(30) & ((\REGDIR|dffs\(6))))) # (\REGDIR|dffs\(31)) ) ) ) # ( !\REGDIR|dffs\(12) & ( \REGDIR|dffs\(4) & ( (!\REGDIR|dffs\(30) & 
-- (\TB[14]~input_o\ & ((!\REGDIR|dffs\(31))))) # (\REGDIR|dffs\(30) & (((\REGDIR|dffs\(31)) # (\REGDIR|dffs\(6))))) ) ) ) # ( \REGDIR|dffs\(12) & ( !\REGDIR|dffs\(4) & ( (!\REGDIR|dffs\(30) & (((\REGDIR|dffs\(31))) # (\TB[14]~input_o\))) # 
-- (\REGDIR|dffs\(30) & (((\REGDIR|dffs\(6) & !\REGDIR|dffs\(31))))) ) ) ) # ( !\REGDIR|dffs\(12) & ( !\REGDIR|dffs\(4) & ( (!\REGDIR|dffs\(31) & ((!\REGDIR|dffs\(30) & (\TB[14]~input_o\)) # (\REGDIR|dffs\(30) & ((\REGDIR|dffs\(6)))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101001100000000010100111111000001010011000011110101001111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_TB[14]~input_o\,
	datab => \REGDIR|ALT_INV_dffs\(6),
	datac => \REGDIR|ALT_INV_dffs\(30),
	datad => \REGDIR|ALT_INV_dffs\(31),
	datae => \REGDIR|ALT_INV_dffs\(12),
	dataf => \REGDIR|ALT_INV_dffs\(4),
	combout => \Mux17~0_combout\);

-- Location: FF_X68_Y4_N53
\REGDIR|dffs[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux17~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGDIR|dffs\(14));

-- Location: IOIBUF_X40_Y0_N1
\TB[16]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_TB(16),
	o => \TB[16]~input_o\);

-- Location: LABCELL_X68_Y4_N24
\Mux15~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux15~0_combout\ = ( \REGDIR|dffs\(31) & ( \REGDIR|dffs\(8) & ( (!\REGDIR|dffs\(30) & (\REGDIR|dffs\(14))) # (\REGDIR|dffs\(30) & ((\REGDIR|dffs\(6)))) ) ) ) # ( !\REGDIR|dffs\(31) & ( \REGDIR|dffs\(8) & ( (\REGDIR|dffs\(30)) # (\TB[16]~input_o\) ) ) ) # 
-- ( \REGDIR|dffs\(31) & ( !\REGDIR|dffs\(8) & ( (!\REGDIR|dffs\(30) & (\REGDIR|dffs\(14))) # (\REGDIR|dffs\(30) & ((\REGDIR|dffs\(6)))) ) ) ) # ( !\REGDIR|dffs\(31) & ( !\REGDIR|dffs\(8) & ( (\TB[16]~input_o\ & !\REGDIR|dffs\(30)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100000000010101010011001100001111111111110101010100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \REGDIR|ALT_INV_dffs\(14),
	datab => \REGDIR|ALT_INV_dffs\(6),
	datac => \ALT_INV_TB[16]~input_o\,
	datad => \REGDIR|ALT_INV_dffs\(30),
	datae => \REGDIR|ALT_INV_dffs\(31),
	dataf => \REGDIR|ALT_INV_dffs\(8),
	combout => \Mux15~0_combout\);

-- Location: FF_X68_Y4_N26
\REGDIR|dffs[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux15~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGDIR|dffs\(16));

-- Location: IOIBUF_X89_Y8_N38
\TB[18]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_TB(18),
	o => \TB[18]~input_o\);

-- Location: LABCELL_X68_Y4_N0
\Mux13~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux13~0_combout\ = ( \REGDIR|dffs\(10) & ( \REGDIR|dffs\(8) & ( ((!\REGDIR|dffs\(31) & ((\TB[18]~input_o\))) # (\REGDIR|dffs\(31) & (\REGDIR|dffs\(16)))) # (\REGDIR|dffs\(30)) ) ) ) # ( !\REGDIR|dffs\(10) & ( \REGDIR|dffs\(8) & ( (!\REGDIR|dffs\(30) & 
-- ((!\REGDIR|dffs\(31) & ((\TB[18]~input_o\))) # (\REGDIR|dffs\(31) & (\REGDIR|dffs\(16))))) # (\REGDIR|dffs\(30) & (((\REGDIR|dffs\(31))))) ) ) ) # ( \REGDIR|dffs\(10) & ( !\REGDIR|dffs\(8) & ( (!\REGDIR|dffs\(30) & ((!\REGDIR|dffs\(31) & 
-- ((\TB[18]~input_o\))) # (\REGDIR|dffs\(31) & (\REGDIR|dffs\(16))))) # (\REGDIR|dffs\(30) & (((!\REGDIR|dffs\(31))))) ) ) ) # ( !\REGDIR|dffs\(10) & ( !\REGDIR|dffs\(8) & ( (!\REGDIR|dffs\(30) & ((!\REGDIR|dffs\(31) & ((\TB[18]~input_o\))) # 
-- (\REGDIR|dffs\(31) & (\REGDIR|dffs\(16))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110001000100001111110100010000001100011101110011111101110111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \REGDIR|ALT_INV_dffs\(16),
	datab => \REGDIR|ALT_INV_dffs\(30),
	datac => \ALT_INV_TB[18]~input_o\,
	datad => \REGDIR|ALT_INV_dffs\(31),
	datae => \REGDIR|ALT_INV_dffs\(10),
	dataf => \REGDIR|ALT_INV_dffs\(8),
	combout => \Mux13~0_combout\);

-- Location: FF_X68_Y4_N2
\REGDIR|dffs[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux13~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGDIR|dffs\(18));

-- Location: IOIBUF_X62_Y0_N1
\TB[20]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_TB(20),
	o => \TB[20]~input_o\);

-- Location: LABCELL_X67_Y3_N24
\Mux11~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux11~0_combout\ = ( \REGDIR|dffs\(30) & ( \REGDIR|dffs\(10) & ( (\REGDIR|dffs\(31)) # (\REGDIR|dffs\(12)) ) ) ) # ( !\REGDIR|dffs\(30) & ( \REGDIR|dffs\(10) & ( (!\REGDIR|dffs\(31) & ((\TB[20]~input_o\))) # (\REGDIR|dffs\(31) & (\REGDIR|dffs\(18))) ) ) 
-- ) # ( \REGDIR|dffs\(30) & ( !\REGDIR|dffs\(10) & ( (\REGDIR|dffs\(12) & !\REGDIR|dffs\(31)) ) ) ) # ( !\REGDIR|dffs\(30) & ( !\REGDIR|dffs\(10) & ( (!\REGDIR|dffs\(31) & ((\TB[20]~input_o\))) # (\REGDIR|dffs\(31) & (\REGDIR|dffs\(18))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001111001111010001000100010000000011110011110111011101110111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \REGDIR|ALT_INV_dffs\(12),
	datab => \REGDIR|ALT_INV_dffs\(31),
	datac => \REGDIR|ALT_INV_dffs\(18),
	datad => \ALT_INV_TB[20]~input_o\,
	datae => \REGDIR|ALT_INV_dffs\(30),
	dataf => \REGDIR|ALT_INV_dffs\(10),
	combout => \Mux11~0_combout\);

-- Location: FF_X67_Y3_N26
\REGDIR|dffs[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux11~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGDIR|dffs\(20));

-- Location: IOIBUF_X56_Y0_N1
\TB[28]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_TB(28),
	o => \TB[28]~input_o\);

-- Location: IOIBUF_X40_Y0_N35
\TB[22]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_TB(22),
	o => \TB[22]~input_o\);

-- Location: LABCELL_X68_Y4_N9
\Mux9~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux9~0_combout\ = ( \REGDIR|dffs\(20) & ( \REGDIR|dffs\(14) & ( (!\REGDIR|dffs\(30) & (((\REGDIR|dffs\(31))) # (\TB[22]~input_o\))) # (\REGDIR|dffs\(30) & (((!\REGDIR|dffs\(31)) # (\REGDIR|dffs\(12))))) ) ) ) # ( !\REGDIR|dffs\(20) & ( \REGDIR|dffs\(14) 
-- & ( (!\REGDIR|dffs\(30) & (\TB[22]~input_o\ & ((!\REGDIR|dffs\(31))))) # (\REGDIR|dffs\(30) & (((!\REGDIR|dffs\(31)) # (\REGDIR|dffs\(12))))) ) ) ) # ( \REGDIR|dffs\(20) & ( !\REGDIR|dffs\(14) & ( (!\REGDIR|dffs\(30) & (((\REGDIR|dffs\(31))) # 
-- (\TB[22]~input_o\))) # (\REGDIR|dffs\(30) & (((\REGDIR|dffs\(12) & \REGDIR|dffs\(31))))) ) ) ) # ( !\REGDIR|dffs\(20) & ( !\REGDIR|dffs\(14) & ( (!\REGDIR|dffs\(30) & (\TB[22]~input_o\ & ((!\REGDIR|dffs\(31))))) # (\REGDIR|dffs\(30) & (((\REGDIR|dffs\(12) 
-- & \REGDIR|dffs\(31))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010001000000101001000101010111101110111000001010111011110101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \REGDIR|ALT_INV_dffs\(30),
	datab => \ALT_INV_TB[22]~input_o\,
	datac => \REGDIR|ALT_INV_dffs\(12),
	datad => \REGDIR|ALT_INV_dffs\(31),
	datae => \REGDIR|ALT_INV_dffs\(20),
	dataf => \REGDIR|ALT_INV_dffs\(14),
	combout => \Mux9~0_combout\);

-- Location: FF_X68_Y4_N11
\REGDIR|dffs[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux9~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGDIR|dffs\(22));

-- Location: IOIBUF_X89_Y35_N78
\TB[24]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_TB(24),
	o => \TB[24]~input_o\);

-- Location: LABCELL_X68_Y4_N30
\Mux7~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux7~0_combout\ = ( \REGDIR|dffs\(30) & ( \REGDIR|dffs\(14) & ( (\REGDIR|dffs\(31)) # (\REGDIR|dffs\(16)) ) ) ) # ( !\REGDIR|dffs\(30) & ( \REGDIR|dffs\(14) & ( (!\REGDIR|dffs\(31) & ((\TB[24]~input_o\))) # (\REGDIR|dffs\(31) & (\REGDIR|dffs\(22))) ) ) ) 
-- # ( \REGDIR|dffs\(30) & ( !\REGDIR|dffs\(14) & ( (\REGDIR|dffs\(16) & !\REGDIR|dffs\(31)) ) ) ) # ( !\REGDIR|dffs\(30) & ( !\REGDIR|dffs\(14) & ( (!\REGDIR|dffs\(31) & ((\TB[24]~input_o\))) # (\REGDIR|dffs\(31) & (\REGDIR|dffs\(22))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001111110011010100000101000000000011111100110101111101011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \REGDIR|ALT_INV_dffs\(16),
	datab => \REGDIR|ALT_INV_dffs\(22),
	datac => \REGDIR|ALT_INV_dffs\(31),
	datad => \ALT_INV_TB[24]~input_o\,
	datae => \REGDIR|ALT_INV_dffs\(30),
	dataf => \REGDIR|ALT_INV_dffs\(14),
	combout => \Mux7~0_combout\);

-- Location: FF_X68_Y4_N32
\REGDIR|dffs[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux7~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGDIR|dffs\(24));

-- Location: IOIBUF_X89_Y38_N38
\TB[26]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_TB(26),
	o => \TB[26]~input_o\);

-- Location: LABCELL_X68_Y4_N42
\Mux5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux5~0_combout\ = ( \REGDIR|dffs\(30) & ( \REGDIR|dffs\(16) & ( (\REGDIR|dffs\(31)) # (\REGDIR|dffs\(18)) ) ) ) # ( !\REGDIR|dffs\(30) & ( \REGDIR|dffs\(16) & ( (!\REGDIR|dffs\(31) & ((\TB[26]~input_o\))) # (\REGDIR|dffs\(31) & (\REGDIR|dffs\(24))) ) ) ) 
-- # ( \REGDIR|dffs\(30) & ( !\REGDIR|dffs\(16) & ( (\REGDIR|dffs\(18) & !\REGDIR|dffs\(31)) ) ) ) # ( !\REGDIR|dffs\(30) & ( !\REGDIR|dffs\(16) & ( (!\REGDIR|dffs\(31) & ((\TB[26]~input_o\))) # (\REGDIR|dffs\(31) & (\REGDIR|dffs\(24))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001111110011010100000101000000000011111100110101111101011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \REGDIR|ALT_INV_dffs\(18),
	datab => \REGDIR|ALT_INV_dffs\(24),
	datac => \REGDIR|ALT_INV_dffs\(31),
	datad => \ALT_INV_TB[26]~input_o\,
	datae => \REGDIR|ALT_INV_dffs\(30),
	dataf => \REGDIR|ALT_INV_dffs\(16),
	combout => \Mux5~0_combout\);

-- Location: FF_X68_Y4_N44
\REGDIR|dffs[26]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux5~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGDIR|dffs\(26));

-- Location: LABCELL_X68_Y4_N18
\Mux3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux3~0_combout\ = ( \REGDIR|dffs\(20) & ( \REGDIR|dffs\(18) & ( ((!\REGDIR|dffs\(31) & (\TB[28]~input_o\)) # (\REGDIR|dffs\(31) & ((\REGDIR|dffs\(26))))) # (\REGDIR|dffs\(30)) ) ) ) # ( !\REGDIR|dffs\(20) & ( \REGDIR|dffs\(18) & ( (!\REGDIR|dffs\(31) & 
-- (\TB[28]~input_o\ & ((!\REGDIR|dffs\(30))))) # (\REGDIR|dffs\(31) & (((\REGDIR|dffs\(30)) # (\REGDIR|dffs\(26))))) ) ) ) # ( \REGDIR|dffs\(20) & ( !\REGDIR|dffs\(18) & ( (!\REGDIR|dffs\(31) & (((\REGDIR|dffs\(30))) # (\TB[28]~input_o\))) # 
-- (\REGDIR|dffs\(31) & (((\REGDIR|dffs\(26) & !\REGDIR|dffs\(30))))) ) ) ) # ( !\REGDIR|dffs\(20) & ( !\REGDIR|dffs\(18) & ( (!\REGDIR|dffs\(30) & ((!\REGDIR|dffs\(31) & (\TB[28]~input_o\)) # (\REGDIR|dffs\(31) & ((\REGDIR|dffs\(26)))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101001100000000010100111111000001010011000011110101001111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_TB[28]~input_o\,
	datab => \REGDIR|ALT_INV_dffs\(26),
	datac => \REGDIR|ALT_INV_dffs\(31),
	datad => \REGDIR|ALT_INV_dffs\(30),
	datae => \REGDIR|ALT_INV_dffs\(20),
	dataf => \REGDIR|ALT_INV_dffs\(18),
	combout => \Mux3~0_combout\);

-- Location: FF_X68_Y4_N20
\REGDIR|dffs[28]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGDIR|dffs\(28));

-- Location: IOIBUF_X70_Y0_N1
\TB[30]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_TB(30),
	o => \TB[30]~input_o\);

-- Location: LABCELL_X67_Y4_N36
\Mux1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux1~0_combout\ = ( \REGDIR|dffs\(30) & ( \REGDIR|dffs\(22) & ( (!\REGDIR|dffs\(31)) # (\REGDIR|dffs\(20)) ) ) ) # ( !\REGDIR|dffs\(30) & ( \REGDIR|dffs\(22) & ( (!\REGDIR|dffs\(31) & ((\TB[30]~input_o\))) # (\REGDIR|dffs\(31) & (\REGDIR|dffs\(28))) ) ) 
-- ) # ( \REGDIR|dffs\(30) & ( !\REGDIR|dffs\(22) & ( (\REGDIR|dffs\(20) & \REGDIR|dffs\(31)) ) ) ) # ( !\REGDIR|dffs\(30) & ( !\REGDIR|dffs\(22) & ( (!\REGDIR|dffs\(31) & ((\TB[30]~input_o\))) # (\REGDIR|dffs\(31) & (\REGDIR|dffs\(28))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001111110011000001010000010100000011111100111111010111110101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \REGDIR|ALT_INV_dffs\(20),
	datab => \REGDIR|ALT_INV_dffs\(28),
	datac => \REGDIR|ALT_INV_dffs\(31),
	datad => \ALT_INV_TB[30]~input_o\,
	datae => \REGDIR|ALT_INV_dffs\(30),
	dataf => \REGDIR|ALT_INV_dffs\(22),
	combout => \Mux1~0_combout\);

-- Location: FF_X67_Y4_N38
\REGDIR|dffs[30]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGDIR|dffs\(30));

-- Location: LABCELL_X67_Y3_N30
\Mux30~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux30~0_combout\ = ( !\REGDIR|dffs\(30) & ( (!\REGDIR|dffs\(31) & \TB[1]~input_o\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110000001100000011000000110000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \REGDIR|ALT_INV_dffs\(31),
	datac => \ALT_INV_TB[1]~input_o\,
	dataf => \REGDIR|ALT_INV_dffs\(30),
	combout => \Mux30~0_combout\);

-- Location: FF_X67_Y3_N32
\REGDIR|dffs[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux30~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGDIR|dffs\(1));

-- Location: IOIBUF_X32_Y0_N52
\TB[3]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_TB(3),
	o => \TB[3]~input_o\);

-- Location: LABCELL_X66_Y3_N39
\Mux28~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux28~0_combout\ = ( \TB[3]~input_o\ & ( (!\REGDIR|dffs\(31)) # (\REGDIR|dffs\(1)) ) ) # ( !\TB[3]~input_o\ & ( (\REGDIR|dffs\(1) & \REGDIR|dffs\(31)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001010101000000000101010111111111010101011111111101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \REGDIR|ALT_INV_dffs\(1),
	datad => \REGDIR|ALT_INV_dffs\(31),
	dataf => \ALT_INV_TB[3]~input_o\,
	combout => \Mux28~0_combout\);

-- Location: FF_X66_Y3_N41
\REGDIR|dffs[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux28~0_combout\,
	sclr => \REGDIR|dffs\(30),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGDIR|dffs\(3));

-- Location: LABCELL_X66_Y3_N36
\Mux26~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux26~0_combout\ = ( \REGDIR|dffs\(3) & ( (\TB[5]~input_o\) # (\REGDIR|dffs\(31)) ) ) # ( !\REGDIR|dffs\(3) & ( (!\REGDIR|dffs\(31) & \TB[5]~input_o\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110000001100000011000000110000111111001111110011111100111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \REGDIR|ALT_INV_dffs\(31),
	datac => \ALT_INV_TB[5]~input_o\,
	dataf => \REGDIR|ALT_INV_dffs\(3),
	combout => \Mux26~0_combout\);

-- Location: FF_X66_Y3_N38
\REGDIR|dffs[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux26~0_combout\,
	sclr => \REGDIR|dffs\(30),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGDIR|dffs\(5));

-- Location: LABCELL_X66_Y3_N45
\Mux24~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux24~0_combout\ = ( \REGDIR|dffs\(5) & ( (\REGDIR|dffs\(31)) # (\TB[7]~input_o\) ) ) # ( !\REGDIR|dffs\(5) & ( (\TB[7]~input_o\ & !\REGDIR|dffs\(31)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100000000000011110000000000001111111111110000111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_TB[7]~input_o\,
	datad => \REGDIR|ALT_INV_dffs\(31),
	dataf => \REGDIR|ALT_INV_dffs\(5),
	combout => \Mux24~0_combout\);

-- Location: FF_X66_Y3_N47
\REGDIR|dffs[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux24~0_combout\,
	sclr => \REGDIR|dffs\(30),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGDIR|dffs\(7));

-- Location: IOIBUF_X4_Y0_N35
\TB[9]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_TB(9),
	o => \TB[9]~input_o\);

-- Location: LABCELL_X66_Y3_N42
\Mux22~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux22~0_combout\ = ( \TB[9]~input_o\ & ( (!\REGDIR|dffs\(31) & (((!\REGDIR|dffs\(30)) # (\REGDIR|dffs\(1))))) # (\REGDIR|dffs\(31) & (\REGDIR|dffs\(7) & ((!\REGDIR|dffs\(30))))) ) ) # ( !\TB[9]~input_o\ & ( (!\REGDIR|dffs\(31) & (((\REGDIR|dffs\(1) & 
-- \REGDIR|dffs\(30))))) # (\REGDIR|dffs\(31) & (\REGDIR|dffs\(7) & ((!\REGDIR|dffs\(30))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000100001010000100010000101010111011000010101011101100001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \REGDIR|ALT_INV_dffs\(31),
	datab => \REGDIR|ALT_INV_dffs\(7),
	datac => \REGDIR|ALT_INV_dffs\(1),
	datad => \REGDIR|ALT_INV_dffs\(30),
	dataf => \ALT_INV_TB[9]~input_o\,
	combout => \Mux22~0_combout\);

-- Location: FF_X66_Y3_N44
\REGDIR|dffs[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux22~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGDIR|dffs\(9));

-- Location: IOIBUF_X6_Y0_N52
\TB[17]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_TB(17),
	o => \TB[17]~input_o\);

-- Location: IOIBUF_X56_Y0_N35
\TB[13]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_TB(13),
	o => \TB[13]~input_o\);

-- Location: IOIBUF_X32_Y0_N35
\TB[11]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_TB(11),
	o => \TB[11]~input_o\);

-- Location: LABCELL_X66_Y3_N48
\Mux20~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux20~0_combout\ = ( \REGDIR|dffs\(30) & ( \REGDIR|dffs\(3) & ( (!\REGDIR|dffs\(31)) # (\REGDIR|dffs\(1)) ) ) ) # ( !\REGDIR|dffs\(30) & ( \REGDIR|dffs\(3) & ( (!\REGDIR|dffs\(31) & (\TB[11]~input_o\)) # (\REGDIR|dffs\(31) & ((\REGDIR|dffs\(9)))) ) ) ) # 
-- ( \REGDIR|dffs\(30) & ( !\REGDIR|dffs\(3) & ( (\REGDIR|dffs\(1) & \REGDIR|dffs\(31)) ) ) ) # ( !\REGDIR|dffs\(30) & ( !\REGDIR|dffs\(3) & ( (!\REGDIR|dffs\(31) & (\TB[11]~input_o\)) # (\REGDIR|dffs\(31) & ((\REGDIR|dffs\(9)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010100110011000000000000111101010101001100111111111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_TB[11]~input_o\,
	datab => \REGDIR|ALT_INV_dffs\(9),
	datac => \REGDIR|ALT_INV_dffs\(1),
	datad => \REGDIR|ALT_INV_dffs\(31),
	datae => \REGDIR|ALT_INV_dffs\(30),
	dataf => \REGDIR|ALT_INV_dffs\(3),
	combout => \Mux20~0_combout\);

-- Location: FF_X66_Y3_N50
\REGDIR|dffs[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux20~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGDIR|dffs\(11));

-- Location: LABCELL_X66_Y3_N30
\Mux18~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux18~0_combout\ = ( \REGDIR|dffs\(11) & ( \REGDIR|dffs\(3) & ( ((!\REGDIR|dffs\(30) & ((\TB[13]~input_o\))) # (\REGDIR|dffs\(30) & (\REGDIR|dffs\(5)))) # (\REGDIR|dffs\(31)) ) ) ) # ( !\REGDIR|dffs\(11) & ( \REGDIR|dffs\(3) & ( (!\REGDIR|dffs\(31) & 
-- ((!\REGDIR|dffs\(30) & ((\TB[13]~input_o\))) # (\REGDIR|dffs\(30) & (\REGDIR|dffs\(5))))) # (\REGDIR|dffs\(31) & (((\REGDIR|dffs\(30))))) ) ) ) # ( \REGDIR|dffs\(11) & ( !\REGDIR|dffs\(3) & ( (!\REGDIR|dffs\(31) & ((!\REGDIR|dffs\(30) & 
-- ((\TB[13]~input_o\))) # (\REGDIR|dffs\(30) & (\REGDIR|dffs\(5))))) # (\REGDIR|dffs\(31) & (((!\REGDIR|dffs\(30))))) ) ) ) # ( !\REGDIR|dffs\(11) & ( !\REGDIR|dffs\(3) & ( (!\REGDIR|dffs\(31) & ((!\REGDIR|dffs\(30) & ((\TB[13]~input_o\))) # 
-- (\REGDIR|dffs\(30) & (\REGDIR|dffs\(5))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011000001010000001111110101000000110000010111110011111101011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \REGDIR|ALT_INV_dffs\(5),
	datab => \ALT_INV_TB[13]~input_o\,
	datac => \REGDIR|ALT_INV_dffs\(31),
	datad => \REGDIR|ALT_INV_dffs\(30),
	datae => \REGDIR|ALT_INV_dffs\(11),
	dataf => \REGDIR|ALT_INV_dffs\(3),
	combout => \Mux18~0_combout\);

-- Location: FF_X66_Y3_N32
\REGDIR|dffs[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux18~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGDIR|dffs\(13));

-- Location: IOIBUF_X34_Y0_N58
\TB[15]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_TB(15),
	o => \TB[15]~input_o\);

-- Location: LABCELL_X66_Y3_N12
\Mux16~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux16~0_combout\ = ( \REGDIR|dffs\(30) & ( \REGDIR|dffs\(7) & ( (!\REGDIR|dffs\(31)) # (\REGDIR|dffs\(5)) ) ) ) # ( !\REGDIR|dffs\(30) & ( \REGDIR|dffs\(7) & ( (!\REGDIR|dffs\(31) & ((\TB[15]~input_o\))) # (\REGDIR|dffs\(31) & (\REGDIR|dffs\(13))) ) ) ) 
-- # ( \REGDIR|dffs\(30) & ( !\REGDIR|dffs\(7) & ( (\REGDIR|dffs\(5) & \REGDIR|dffs\(31)) ) ) ) # ( !\REGDIR|dffs\(30) & ( !\REGDIR|dffs\(7) & ( (!\REGDIR|dffs\(31) & ((\TB[15]~input_o\))) # (\REGDIR|dffs\(31) & (\REGDIR|dffs\(13))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100110011000000000101010100001111001100111111111101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \REGDIR|ALT_INV_dffs\(5),
	datab => \REGDIR|ALT_INV_dffs\(13),
	datac => \ALT_INV_TB[15]~input_o\,
	datad => \REGDIR|ALT_INV_dffs\(31),
	datae => \REGDIR|ALT_INV_dffs\(30),
	dataf => \REGDIR|ALT_INV_dffs\(7),
	combout => \Mux16~0_combout\);

-- Location: FF_X66_Y3_N14
\REGDIR|dffs[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux16~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGDIR|dffs\(15));

-- Location: LABCELL_X66_Y3_N54
\Mux14~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux14~0_combout\ = ( \REGDIR|dffs\(15) & ( \REGDIR|dffs\(7) & ( ((!\REGDIR|dffs\(30) & ((\TB[17]~input_o\))) # (\REGDIR|dffs\(30) & (\REGDIR|dffs\(9)))) # (\REGDIR|dffs\(31)) ) ) ) # ( !\REGDIR|dffs\(15) & ( \REGDIR|dffs\(7) & ( (!\REGDIR|dffs\(31) & 
-- ((!\REGDIR|dffs\(30) & ((\TB[17]~input_o\))) # (\REGDIR|dffs\(30) & (\REGDIR|dffs\(9))))) # (\REGDIR|dffs\(31) & (((\REGDIR|dffs\(30))))) ) ) ) # ( \REGDIR|dffs\(15) & ( !\REGDIR|dffs\(7) & ( (!\REGDIR|dffs\(31) & ((!\REGDIR|dffs\(30) & 
-- ((\TB[17]~input_o\))) # (\REGDIR|dffs\(30) & (\REGDIR|dffs\(9))))) # (\REGDIR|dffs\(31) & (((!\REGDIR|dffs\(30))))) ) ) ) # ( !\REGDIR|dffs\(15) & ( !\REGDIR|dffs\(7) & ( (!\REGDIR|dffs\(31) & ((!\REGDIR|dffs\(30) & ((\TB[17]~input_o\))) # 
-- (\REGDIR|dffs\(30) & (\REGDIR|dffs\(9))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001010100010010100101111001000000111101001110101011111110111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \REGDIR|ALT_INV_dffs\(31),
	datab => \REGDIR|ALT_INV_dffs\(9),
	datac => \REGDIR|ALT_INV_dffs\(30),
	datad => \ALT_INV_TB[17]~input_o\,
	datae => \REGDIR|ALT_INV_dffs\(15),
	dataf => \REGDIR|ALT_INV_dffs\(7),
	combout => \Mux14~0_combout\);

-- Location: FF_X66_Y3_N56
\REGDIR|dffs[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux14~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGDIR|dffs\(17));

-- Location: IOIBUF_X36_Y0_N52
\TB[19]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_TB(19),
	o => \TB[19]~input_o\);

-- Location: LABCELL_X66_Y3_N0
\Mux12~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux12~0_combout\ = ( \REGDIR|dffs\(11) & ( \REGDIR|dffs\(9) & ( ((!\REGDIR|dffs\(31) & ((\TB[19]~input_o\))) # (\REGDIR|dffs\(31) & (\REGDIR|dffs\(17)))) # (\REGDIR|dffs\(30)) ) ) ) # ( !\REGDIR|dffs\(11) & ( \REGDIR|dffs\(9) & ( (!\REGDIR|dffs\(30) & 
-- ((!\REGDIR|dffs\(31) & ((\TB[19]~input_o\))) # (\REGDIR|dffs\(31) & (\REGDIR|dffs\(17))))) # (\REGDIR|dffs\(30) & (((\REGDIR|dffs\(31))))) ) ) ) # ( \REGDIR|dffs\(11) & ( !\REGDIR|dffs\(9) & ( (!\REGDIR|dffs\(30) & ((!\REGDIR|dffs\(31) & 
-- ((\TB[19]~input_o\))) # (\REGDIR|dffs\(31) & (\REGDIR|dffs\(17))))) # (\REGDIR|dffs\(30) & (((!\REGDIR|dffs\(31))))) ) ) ) # ( !\REGDIR|dffs\(11) & ( !\REGDIR|dffs\(9) & ( (!\REGDIR|dffs\(30) & ((!\REGDIR|dffs\(31) & ((\TB[19]~input_o\))) # 
-- (\REGDIR|dffs\(31) & (\REGDIR|dffs\(17))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110001000100001111110100010000001100011101110011111101110111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \REGDIR|ALT_INV_dffs\(17),
	datab => \REGDIR|ALT_INV_dffs\(30),
	datac => \ALT_INV_TB[19]~input_o\,
	datad => \REGDIR|ALT_INV_dffs\(31),
	datae => \REGDIR|ALT_INV_dffs\(11),
	dataf => \REGDIR|ALT_INV_dffs\(9),
	combout => \Mux12~0_combout\);

-- Location: FF_X66_Y3_N2
\REGDIR|dffs[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux12~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGDIR|dffs\(19));

-- Location: IOIBUF_X30_Y0_N35
\TB[21]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_TB(21),
	o => \TB[21]~input_o\);

-- Location: LABCELL_X66_Y3_N6
\Mux10~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux10~0_combout\ = ( \REGDIR|dffs\(11) & ( \REGDIR|dffs\(30) & ( (\REGDIR|dffs\(31)) # (\REGDIR|dffs\(13)) ) ) ) # ( !\REGDIR|dffs\(11) & ( \REGDIR|dffs\(30) & ( (\REGDIR|dffs\(13) & !\REGDIR|dffs\(31)) ) ) ) # ( \REGDIR|dffs\(11) & ( !\REGDIR|dffs\(30) 
-- & ( (!\REGDIR|dffs\(31) & ((\TB[21]~input_o\))) # (\REGDIR|dffs\(31) & (\REGDIR|dffs\(19))) ) ) ) # ( !\REGDIR|dffs\(11) & ( !\REGDIR|dffs\(30) & ( (!\REGDIR|dffs\(31) & ((\TB[21]~input_o\))) # (\REGDIR|dffs\(31) & (\REGDIR|dffs\(19))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111101010101000011110101010100110011000000000011001111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \REGDIR|ALT_INV_dffs\(19),
	datab => \REGDIR|ALT_INV_dffs\(13),
	datac => \ALT_INV_TB[21]~input_o\,
	datad => \REGDIR|ALT_INV_dffs\(31),
	datae => \REGDIR|ALT_INV_dffs\(11),
	dataf => \REGDIR|ALT_INV_dffs\(30),
	combout => \Mux10~0_combout\);

-- Location: FF_X66_Y3_N8
\REGDIR|dffs[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux10~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGDIR|dffs\(21));

-- Location: IOIBUF_X89_Y35_N95
\TB[27]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_TB(27),
	o => \TB[27]~input_o\);

-- Location: IOIBUF_X34_Y0_N75
\TB[23]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_TB(23),
	o => \TB[23]~input_o\);

-- Location: LABCELL_X66_Y3_N24
\Mux8~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux8~0_combout\ = ( \REGDIR|dffs\(13) & ( \REGDIR|dffs\(15) & ( ((!\REGDIR|dffs\(31) & ((\TB[23]~input_o\))) # (\REGDIR|dffs\(31) & (\REGDIR|dffs\(21)))) # (\REGDIR|dffs\(30)) ) ) ) # ( !\REGDIR|dffs\(13) & ( \REGDIR|dffs\(15) & ( (!\REGDIR|dffs\(31) & 
-- (((\REGDIR|dffs\(30)) # (\TB[23]~input_o\)))) # (\REGDIR|dffs\(31) & (\REGDIR|dffs\(21) & ((!\REGDIR|dffs\(30))))) ) ) ) # ( \REGDIR|dffs\(13) & ( !\REGDIR|dffs\(15) & ( (!\REGDIR|dffs\(31) & (((\TB[23]~input_o\ & !\REGDIR|dffs\(30))))) # 
-- (\REGDIR|dffs\(31) & (((\REGDIR|dffs\(30))) # (\REGDIR|dffs\(21)))) ) ) ) # ( !\REGDIR|dffs\(13) & ( !\REGDIR|dffs\(15) & ( (!\REGDIR|dffs\(30) & ((!\REGDIR|dffs\(31) & ((\TB[23]~input_o\))) # (\REGDIR|dffs\(31) & (\REGDIR|dffs\(21))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001101100000000000110110101010100011011101010100001101111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \REGDIR|ALT_INV_dffs\(31),
	datab => \REGDIR|ALT_INV_dffs\(21),
	datac => \ALT_INV_TB[23]~input_o\,
	datad => \REGDIR|ALT_INV_dffs\(30),
	datae => \REGDIR|ALT_INV_dffs\(13),
	dataf => \REGDIR|ALT_INV_dffs\(15),
	combout => \Mux8~0_combout\);

-- Location: FF_X66_Y3_N26
\REGDIR|dffs[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux8~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGDIR|dffs\(23));

-- Location: IOIBUF_X2_Y0_N58
\TB[25]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_TB(25),
	o => \TB[25]~input_o\);

-- Location: LABCELL_X66_Y3_N18
\Mux6~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux6~0_combout\ = ( \REGDIR|dffs\(30) & ( \REGDIR|dffs\(17) & ( (!\REGDIR|dffs\(31)) # (\REGDIR|dffs\(15)) ) ) ) # ( !\REGDIR|dffs\(30) & ( \REGDIR|dffs\(17) & ( (!\REGDIR|dffs\(31) & ((\TB[25]~input_o\))) # (\REGDIR|dffs\(31) & (\REGDIR|dffs\(23))) ) ) 
-- ) # ( \REGDIR|dffs\(30) & ( !\REGDIR|dffs\(17) & ( (\REGDIR|dffs\(15) & \REGDIR|dffs\(31)) ) ) ) # ( !\REGDIR|dffs\(30) & ( !\REGDIR|dffs\(17) & ( (!\REGDIR|dffs\(31) & ((\TB[25]~input_o\))) # (\REGDIR|dffs\(31) & (\REGDIR|dffs\(23))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111101010101000000000011001100001111010101011111111100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \REGDIR|ALT_INV_dffs\(23),
	datab => \REGDIR|ALT_INV_dffs\(15),
	datac => \ALT_INV_TB[25]~input_o\,
	datad => \REGDIR|ALT_INV_dffs\(31),
	datae => \REGDIR|ALT_INV_dffs\(30),
	dataf => \REGDIR|ALT_INV_dffs\(17),
	combout => \Mux6~0_combout\);

-- Location: FF_X66_Y3_N20
\REGDIR|dffs[25]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux6~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGDIR|dffs\(25));

-- Location: LABCELL_X67_Y3_N6
\Mux4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux4~0_combout\ = ( \REGDIR|dffs\(19) & ( \REGDIR|dffs\(17) & ( ((!\REGDIR|dffs\(31) & (\TB[27]~input_o\)) # (\REGDIR|dffs\(31) & ((\REGDIR|dffs\(25))))) # (\REGDIR|dffs\(30)) ) ) ) # ( !\REGDIR|dffs\(19) & ( \REGDIR|dffs\(17) & ( (!\REGDIR|dffs\(31) & 
-- (\TB[27]~input_o\ & ((!\REGDIR|dffs\(30))))) # (\REGDIR|dffs\(31) & (((\REGDIR|dffs\(30)) # (\REGDIR|dffs\(25))))) ) ) ) # ( \REGDIR|dffs\(19) & ( !\REGDIR|dffs\(17) & ( (!\REGDIR|dffs\(31) & (((\REGDIR|dffs\(30))) # (\TB[27]~input_o\))) # 
-- (\REGDIR|dffs\(31) & (((\REGDIR|dffs\(25) & !\REGDIR|dffs\(30))))) ) ) ) # ( !\REGDIR|dffs\(19) & ( !\REGDIR|dffs\(17) & ( (!\REGDIR|dffs\(30) & ((!\REGDIR|dffs\(31) & (\TB[27]~input_o\)) # (\REGDIR|dffs\(31) & ((\REGDIR|dffs\(25)))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100011100000000010001111100110001000111001100110100011111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_TB[27]~input_o\,
	datab => \REGDIR|ALT_INV_dffs\(31),
	datac => \REGDIR|ALT_INV_dffs\(25),
	datad => \REGDIR|ALT_INV_dffs\(30),
	datae => \REGDIR|ALT_INV_dffs\(19),
	dataf => \REGDIR|ALT_INV_dffs\(17),
	combout => \Mux4~0_combout\);

-- Location: FF_X67_Y3_N8
\REGDIR|dffs[27]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux4~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGDIR|dffs\(27));

-- Location: IOIBUF_X50_Y0_N75
\TB[29]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_TB(29),
	o => \TB[29]~input_o\);

-- Location: LABCELL_X67_Y3_N51
\Mux2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux2~0_combout\ = ( \REGDIR|dffs\(30) & ( \REGDIR|dffs\(31) & ( \REGDIR|dffs\(19) ) ) ) # ( !\REGDIR|dffs\(30) & ( \REGDIR|dffs\(31) & ( \REGDIR|dffs\(27) ) ) ) # ( \REGDIR|dffs\(30) & ( !\REGDIR|dffs\(31) & ( \REGDIR|dffs\(21) ) ) ) # ( 
-- !\REGDIR|dffs\(30) & ( !\REGDIR|dffs\(31) & ( \TB[29]~input_o\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111010101010101010100110011001100110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \REGDIR|ALT_INV_dffs\(21),
	datab => \REGDIR|ALT_INV_dffs\(27),
	datac => \ALT_INV_TB[29]~input_o\,
	datad => \REGDIR|ALT_INV_dffs\(19),
	datae => \REGDIR|ALT_INV_dffs\(30),
	dataf => \REGDIR|ALT_INV_dffs\(31),
	combout => \Mux2~0_combout\);

-- Location: FF_X67_Y3_N53
\REGDIR|dffs[29]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGDIR|dffs\(29));

-- Location: IOIBUF_X89_Y9_N21
\TB[31]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_TB(31),
	o => \TB[31]~input_o\);

-- Location: LABCELL_X67_Y4_N48
\Mux0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux0~0_combout\ = ( \REGDIR|dffs\(31) & ( \REGDIR|dffs\(23) & ( (!\REGDIR|dffs\(30) & (\REGDIR|dffs\(29))) # (\REGDIR|dffs\(30) & ((\REGDIR|dffs\(21)))) ) ) ) # ( !\REGDIR|dffs\(31) & ( \REGDIR|dffs\(23) & ( (\REGDIR|dffs\(30)) # (\TB[31]~input_o\) ) ) ) 
-- # ( \REGDIR|dffs\(31) & ( !\REGDIR|dffs\(23) & ( (!\REGDIR|dffs\(30) & (\REGDIR|dffs\(29))) # (\REGDIR|dffs\(30) & ((\REGDIR|dffs\(21)))) ) ) ) # ( !\REGDIR|dffs\(31) & ( !\REGDIR|dffs\(23) & ( (\TB[31]~input_o\ & !\REGDIR|dffs\(30)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011000000110000010100000101111100111111001111110101000001011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \REGDIR|ALT_INV_dffs\(29),
	datab => \ALT_INV_TB[31]~input_o\,
	datac => \REGDIR|ALT_INV_dffs\(30),
	datad => \REGDIR|ALT_INV_dffs\(21),
	datae => \REGDIR|ALT_INV_dffs\(31),
	dataf => \REGDIR|ALT_INV_dffs\(23),
	combout => \Mux0~0_combout\);

-- Location: FF_X67_Y4_N50
\REGDIR|dffs[31]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGDIR|dffs\(31));

-- Location: LABCELL_X66_Y4_N48
CLKP : cyclonev_lcell_comb
-- Equation(s):
-- \CLKP~combout\ = LCELL(( \REGDIR|dffs\(30) & ( \CLK~input_o\ ) ) # ( !\REGDIR|dffs\(30) & ( (\REGDIR|dffs\(31) & \CLK~input_o\) ) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100000011000000110000001100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \REGDIR|ALT_INV_dffs\(31),
	datac => \ALT_INV_CLK~input_o\,
	dataf => \REGDIR|ALT_INV_dffs\(30),
	combout => \CLKP~combout\);

-- Location: IOIBUF_X89_Y36_N21
\S1[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_S1(0),
	o => \S1[0]~input_o\);

-- Location: LABCELL_X67_Y4_N6
\Mux39~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux39~0_combout\ = ( !\REGDIR|dffs\(31) & ( \S1[0]~input_o\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_S1[0]~input_o\,
	dataf => \REGDIR|ALT_INV_dffs\(31),
	combout => \Mux39~0_combout\);

-- Location: LABCELL_X67_Y4_N0
\REGS1|dffs[0]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \REGS1|dffs[0]~0_combout\ = ( \REGDIR|dffs\(31) ) # ( !\REGDIR|dffs\(31) & ( !\REGDIR|dffs\(30) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011110000111100001111000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \REGDIR|ALT_INV_dffs\(30),
	dataf => \REGDIR|ALT_INV_dffs\(31),
	combout => \REGS1|dffs[0]~0_combout\);

-- Location: FF_X67_Y4_N8
\REGS1|dffs[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux39~0_combout\,
	ena => \REGS1|dffs[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGS1|dffs\(0));

-- Location: IOIBUF_X40_Y0_N18
\S1[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_S1(2),
	o => \S1[2]~input_o\);

-- Location: LABCELL_X67_Y4_N12
\Mux37~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux37~0_combout\ = ( \REGDIR|dffs\(31) & ( \REGS1|dffs\(0) ) ) # ( !\REGDIR|dffs\(31) & ( \S1[2]~input_o\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \REGS1|ALT_INV_dffs\(0),
	datad => \ALT_INV_S1[2]~input_o\,
	dataf => \REGDIR|ALT_INV_dffs\(31),
	combout => \Mux37~0_combout\);

-- Location: FF_X67_Y4_N14
\REGS1|dffs[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux37~0_combout\,
	ena => \REGS1|dffs[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGS1|dffs\(2));

-- Location: IOIBUF_X62_Y81_N18
\S1[4]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_S1(4),
	o => \S1[4]~input_o\);

-- Location: LABCELL_X67_Y4_N33
\Mux35~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux35~0_combout\ = ( \REGDIR|dffs\(31) & ( \REGS1|dffs\(2) ) ) # ( !\REGDIR|dffs\(31) & ( \S1[4]~input_o\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \REGS1|ALT_INV_dffs\(2),
	datac => \ALT_INV_S1[4]~input_o\,
	dataf => \REGDIR|ALT_INV_dffs\(31),
	combout => \Mux35~0_combout\);

-- Location: FF_X67_Y4_N35
\REGS1|dffs[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux35~0_combout\,
	ena => \REGS1|dffs[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGS1|dffs\(4));

-- Location: IOIBUF_X89_Y38_N4
\S1[6]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_S1(6),
	o => \S1[6]~input_o\);

-- Location: LABCELL_X67_Y4_N21
\Mux33~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux33~0_combout\ = ( \REGDIR|dffs\(31) & ( \REGS1|dffs\(4) ) ) # ( !\REGDIR|dffs\(31) & ( \S1[6]~input_o\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \REGS1|ALT_INV_dffs\(4),
	datac => \ALT_INV_S1[6]~input_o\,
	dataf => \REGDIR|ALT_INV_dffs\(31),
	combout => \Mux33~0_combout\);

-- Location: FF_X67_Y4_N23
\REGS1|dffs[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux33~0_combout\,
	ena => \REGS1|dffs[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGS1|dffs\(6));

-- Location: LABCELL_X66_Y4_N45
\DA1[18]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \DA1[18]~0_combout\ = ( \REGS1|dffs\(6) & ( \REGDIR|dffs\(31) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \REGS1|ALT_INV_dffs\(6),
	dataf => \REGDIR|ALT_INV_dffs\(31),
	combout => \DA1[18]~0_combout\);

-- Location: FF_X66_Y4_N46
\REGA1|dffs[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	d => \DA1[18]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA1|dffs\(18));

-- Location: FF_X66_Y4_N43
\REGA1|dffs[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	asdata => \REGA1|dffs\(18),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA1|dffs\(15));

-- Location: LABCELL_X66_Y4_N15
\REGA1|dffs[12]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \REGA1|dffs[12]~feeder_combout\ = \REGA1|dffs\(15)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \REGA1|ALT_INV_dffs\(15),
	combout => \REGA1|dffs[12]~feeder_combout\);

-- Location: FF_X66_Y4_N17
\REGA1|dffs[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	d => \REGA1|dffs[12]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA1|dffs\(12));

-- Location: FF_X66_Y4_N50
\REGA1|dffs[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	asdata => \REGA1|dffs\(12),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA1|dffs\(9));

-- Location: FF_X65_Y4_N37
\REGA1|dffs[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	asdata => \REGA1|dffs\(9),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA1|dffs\(6));

-- Location: MLABCELL_X65_Y4_N21
\REGA1|dffs[3]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \REGA1|dffs[3]~feeder_combout\ = \REGA1|dffs\(6)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \REGA1|ALT_INV_dffs\(6),
	combout => \REGA1|dffs[3]~feeder_combout\);

-- Location: FF_X65_Y4_N22
\REGA1|dffs[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	d => \REGA1|dffs[3]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA1|dffs\(3));

-- Location: LABCELL_X64_Y4_N39
\REGA1|dffs[0]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \REGA1|dffs[0]~feeder_combout\ = \REGA1|dffs\(3)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \REGA1|ALT_INV_dffs\(3),
	combout => \REGA1|dffs[0]~feeder_combout\);

-- Location: FF_X64_Y4_N40
\REGA1|dffs[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	d => \REGA1|dffs[0]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA1|dffs\(0));

-- Location: IOIBUF_X89_Y6_N21
\S1[7]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_S1(7),
	o => \S1[7]~input_o\);

-- Location: IOIBUF_X68_Y81_N18
\S1[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_S1(1),
	o => \S1[1]~input_o\);

-- Location: LABCELL_X68_Y4_N15
\Mux38~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux38~0_combout\ = ( !\REGDIR|dffs\(31) & ( \S1[1]~input_o\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_S1[1]~input_o\,
	dataf => \REGDIR|ALT_INV_dffs\(31),
	combout => \Mux38~0_combout\);

-- Location: FF_X67_Y4_N20
\REGS1|dffs[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	asdata => \Mux38~0_combout\,
	sload => VCC,
	ena => \REGS1|dffs[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGS1|dffs\(1));

-- Location: IOIBUF_X89_Y36_N38
\S1[3]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_S1(3),
	o => \S1[3]~input_o\);

-- Location: LABCELL_X67_Y4_N30
\Mux36~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux36~0_combout\ = ( \REGDIR|dffs\(31) & ( \REGS1|dffs\(1) ) ) # ( !\REGDIR|dffs\(31) & ( \S1[3]~input_o\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \REGS1|ALT_INV_dffs\(1),
	datad => \ALT_INV_S1[3]~input_o\,
	dataf => \REGDIR|ALT_INV_dffs\(31),
	combout => \Mux36~0_combout\);

-- Location: FF_X67_Y4_N32
\REGS1|dffs[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux36~0_combout\,
	ena => \REGS1|dffs[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGS1|dffs\(3));

-- Location: IOIBUF_X26_Y0_N92
\S1[5]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_S1(5),
	o => \S1[5]~input_o\);

-- Location: LABCELL_X67_Y4_N9
\Mux34~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux34~0_combout\ = ( \REGDIR|dffs\(31) & ( \REGS1|dffs\(3) ) ) # ( !\REGDIR|dffs\(31) & ( \S1[5]~input_o\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \REGS1|ALT_INV_dffs\(3),
	datac => \ALT_INV_S1[5]~input_o\,
	dataf => \REGDIR|ALT_INV_dffs\(31),
	combout => \Mux34~0_combout\);

-- Location: FF_X67_Y4_N11
\REGS1|dffs[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux34~0_combout\,
	ena => \REGS1|dffs[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGS1|dffs\(5));

-- Location: LABCELL_X67_Y4_N15
\Mux32~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux32~0_combout\ = ( \REGDIR|dffs\(31) & ( \REGS1|dffs\(5) ) ) # ( !\REGDIR|dffs\(31) & ( \S1[7]~input_o\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010100000000111111110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_S1[7]~input_o\,
	datad => \REGS1|ALT_INV_dffs\(5),
	dataf => \REGDIR|ALT_INV_dffs\(31),
	combout => \Mux32~0_combout\);

-- Location: FF_X67_Y4_N17
\REGS1|dffs[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux32~0_combout\,
	ena => \REGS1|dffs[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGS1|dffs\(7));

-- Location: LABCELL_X66_Y4_N30
\DA1[19]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \DA1[19]~1_combout\ = ( \REGDIR|dffs\(31) & ( \REGS1|dffs\(7) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \REGS1|ALT_INV_dffs\(7),
	dataf => \REGDIR|ALT_INV_dffs\(31),
	combout => \DA1[19]~1_combout\);

-- Location: FF_X66_Y4_N31
\REGA1|dffs[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	d => \DA1[19]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA1|dffs\(19));

-- Location: LABCELL_X66_Y4_N3
\REGA1|dffs[16]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \REGA1|dffs[16]~feeder_combout\ = ( \REGA1|dffs\(19) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \REGA1|ALT_INV_dffs\(19),
	combout => \REGA1|dffs[16]~feeder_combout\);

-- Location: FF_X66_Y4_N4
\REGA1|dffs[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	d => \REGA1|dffs[16]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA1|dffs\(16));

-- Location: MLABCELL_X65_Y4_N39
\REGA1|dffs[13]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \REGA1|dffs[13]~feeder_combout\ = \REGA1|dffs\(16)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \REGA1|ALT_INV_dffs\(16),
	combout => \REGA1|dffs[13]~feeder_combout\);

-- Location: FF_X65_Y4_N40
\REGA1|dffs[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	d => \REGA1|dffs[13]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA1|dffs\(13));

-- Location: MLABCELL_X65_Y4_N42
\REGA1|dffs[10]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \REGA1|dffs[10]~feeder_combout\ = \REGA1|dffs\(13)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \REGA1|ALT_INV_dffs\(13),
	combout => \REGA1|dffs[10]~feeder_combout\);

-- Location: FF_X65_Y4_N43
\REGA1|dffs[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	d => \REGA1|dffs[10]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA1|dffs\(10));

-- Location: MLABCELL_X65_Y4_N0
\REGA1|dffs[7]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \REGA1|dffs[7]~feeder_combout\ = \REGA1|dffs\(10)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \REGA1|ALT_INV_dffs\(10),
	combout => \REGA1|dffs[7]~feeder_combout\);

-- Location: FF_X65_Y4_N1
\REGA1|dffs[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	d => \REGA1|dffs[7]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA1|dffs\(7));

-- Location: MLABCELL_X65_Y4_N3
\REGA1|dffs[4]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \REGA1|dffs[4]~feeder_combout\ = \REGA1|dffs\(7)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \REGA1|ALT_INV_dffs\(7),
	combout => \REGA1|dffs[4]~feeder_combout\);

-- Location: FF_X65_Y4_N5
\REGA1|dffs[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	d => \REGA1|dffs[4]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA1|dffs\(4));

-- Location: MLABCELL_X65_Y4_N48
\REGA1|dffs[1]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \REGA1|dffs[1]~feeder_combout\ = \REGA1|dffs\(4)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \REGA1|ALT_INV_dffs\(4),
	combout => \REGA1|dffs[1]~feeder_combout\);

-- Location: FF_X65_Y4_N49
\REGA1|dffs[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	d => \REGA1|dffs[1]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA1|dffs\(1));

-- Location: LABCELL_X66_Y4_N51
\REGA1|dffs[20]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \REGA1|dffs[20]~0_combout\ = ( !\REGDIR|dffs\(31) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \REGDIR|ALT_INV_dffs\(31),
	combout => \REGA1|dffs[20]~0_combout\);

-- Location: FF_X66_Y4_N53
\REGA1|dffs[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	d => \REGA1|dffs[20]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA1|dffs\(20));

-- Location: LABCELL_X66_Y4_N18
\REGA1|dffs[17]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \REGA1|dffs[17]~feeder_combout\ = \REGA1|dffs\(20)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \REGA1|ALT_INV_dffs\(20),
	combout => \REGA1|dffs[17]~feeder_combout\);

-- Location: FF_X66_Y4_N19
\REGA1|dffs[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	d => \REGA1|dffs[17]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA1|dffs\(17));

-- Location: LABCELL_X66_Y4_N21
\REGA1|dffs[14]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \REGA1|dffs[14]~feeder_combout\ = ( \REGA1|dffs\(17) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \REGA1|ALT_INV_dffs\(17),
	combout => \REGA1|dffs[14]~feeder_combout\);

-- Location: FF_X66_Y4_N23
\REGA1|dffs[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	d => \REGA1|dffs[14]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA1|dffs\(14));

-- Location: MLABCELL_X65_Y4_N27
\REGA1|dffs[11]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \REGA1|dffs[11]~feeder_combout\ = \REGA1|dffs\(14)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \REGA1|ALT_INV_dffs\(14),
	combout => \REGA1|dffs[11]~feeder_combout\);

-- Location: FF_X65_Y4_N29
\REGA1|dffs[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	d => \REGA1|dffs[11]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA1|dffs\(11));

-- Location: MLABCELL_X65_Y4_N51
\REGA1|dffs[8]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \REGA1|dffs[8]~feeder_combout\ = \REGA1|dffs\(11)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \REGA1|ALT_INV_dffs\(11),
	combout => \REGA1|dffs[8]~feeder_combout\);

-- Location: FF_X65_Y4_N53
\REGA1|dffs[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	d => \REGA1|dffs[8]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA1|dffs\(8));

-- Location: MLABCELL_X65_Y4_N9
\REGA1|dffs[5]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \REGA1|dffs[5]~feeder_combout\ = \REGA1|dffs\(8)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \REGA1|ALT_INV_dffs\(8),
	combout => \REGA1|dffs[5]~feeder_combout\);

-- Location: FF_X65_Y4_N10
\REGA1|dffs[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	d => \REGA1|dffs[5]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA1|dffs\(5));

-- Location: LABCELL_X64_Y4_N45
\REGA1|dffs[2]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \REGA1|dffs[2]~feeder_combout\ = \REGA1|dffs\(5)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \REGA1|ALT_INV_dffs\(5),
	combout => \REGA1|dffs[2]~feeder_combout\);

-- Location: FF_X64_Y4_N46
\REGA1|dffs[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	d => \REGA1|dffs[2]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA1|dffs\(2));

-- Location: IOIBUF_X68_Y81_N1
\S2[4]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_S2(4),
	o => \S2[4]~input_o\);

-- Location: IOIBUF_X8_Y0_N1
\S2[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_S2(2),
	o => \S2[2]~input_o\);

-- Location: IOIBUF_X8_Y0_N52
\S2[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_S2(0),
	o => \S2[0]~input_o\);

-- Location: LABCELL_X67_Y4_N54
\Mux47~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux47~0_combout\ = ( !\REGDIR|dffs\(30) & ( \S2[0]~input_o\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_S2[0]~input_o\,
	dataf => \REGDIR|ALT_INV_dffs\(30),
	combout => \Mux47~0_combout\);

-- Location: LABCELL_X67_Y4_N24
\REGS2|dffs[0]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \REGS2|dffs[0]~0_combout\ = ( \REGDIR|dffs\(31) & ( \REGDIR|dffs\(30) ) ) # ( !\REGDIR|dffs\(31) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \REGDIR|ALT_INV_dffs\(30),
	dataf => \REGDIR|ALT_INV_dffs\(31),
	combout => \REGS2|dffs[0]~0_combout\);

-- Location: FF_X67_Y4_N56
\REGS2|dffs[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux47~0_combout\,
	ena => \REGS2|dffs[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGS2|dffs\(0));

-- Location: LABCELL_X67_Y4_N27
\Mux45~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux45~0_combout\ = ( \REGS2|dffs\(0) & ( (\S2[2]~input_o\) # (\REGDIR|dffs\(30)) ) ) # ( !\REGS2|dffs\(0) & ( (!\REGDIR|dffs\(30) & \S2[2]~input_o\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000101000001010000010100000101001011111010111110101111101011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \REGDIR|ALT_INV_dffs\(30),
	datac => \ALT_INV_S2[2]~input_o\,
	dataf => \REGS2|ALT_INV_dffs\(0),
	combout => \Mux45~0_combout\);

-- Location: FF_X67_Y4_N29
\REGS2|dffs[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux45~0_combout\,
	ena => \REGS2|dffs[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGS2|dffs\(2));

-- Location: LABCELL_X67_Y4_N45
\Mux43~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux43~0_combout\ = ( \REGDIR|dffs\(30) & ( \REGS2|dffs\(2) ) ) # ( !\REGDIR|dffs\(30) & ( \S2[4]~input_o\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100000000111111110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_S2[4]~input_o\,
	datad => \REGS2|ALT_INV_dffs\(2),
	dataf => \REGDIR|ALT_INV_dffs\(30),
	combout => \Mux43~0_combout\);

-- Location: FF_X67_Y4_N47
\REGS2|dffs[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux43~0_combout\,
	ena => \REGS2|dffs[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGS2|dffs\(4));

-- Location: IOIBUF_X8_Y0_N18
\S2[6]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_S2(6),
	o => \S2[6]~input_o\);

-- Location: LABCELL_X67_Y4_N3
\Mux41~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux41~0_combout\ = ( \REGDIR|dffs\(30) & ( \REGS2|dffs\(4) ) ) # ( !\REGDIR|dffs\(30) & ( \S2[6]~input_o\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \REGS2|ALT_INV_dffs\(4),
	datad => \ALT_INV_S2[6]~input_o\,
	dataf => \REGDIR|ALT_INV_dffs\(30),
	combout => \Mux41~0_combout\);

-- Location: FF_X67_Y4_N5
\REGS2|dffs[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux41~0_combout\,
	ena => \REGS2|dffs[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGS2|dffs\(6));

-- Location: LABCELL_X66_Y4_N36
\DA2[18]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \DA2[18]~0_combout\ = ( \REGS2|dffs\(6) & ( \REGDIR|dffs\(30) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \REGS2|ALT_INV_dffs\(6),
	dataf => \REGDIR|ALT_INV_dffs\(30),
	combout => \DA2[18]~0_combout\);

-- Location: FF_X66_Y4_N37
\REGA2|dffs[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	d => \DA2[18]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA2|dffs\(18));

-- Location: FF_X66_Y4_N55
\REGA2|dffs[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	asdata => \REGA2|dffs\(18),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA2|dffs\(15));

-- Location: FF_X66_Y4_N59
\REGA2|dffs[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	asdata => \REGA2|dffs\(15),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA2|dffs\(12));

-- Location: FF_X66_Y4_N40
\REGA2|dffs[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	asdata => \REGA2|dffs\(12),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA2|dffs\(9));

-- Location: FF_X66_Y4_N7
\REGA2|dffs[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	asdata => \REGA2|dffs\(9),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA2|dffs\(6));

-- Location: LABCELL_X66_Y4_N9
\REGA2|dffs[3]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \REGA2|dffs[3]~feeder_combout\ = \REGA2|dffs\(6)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \REGA2|ALT_INV_dffs\(6),
	combout => \REGA2|dffs[3]~feeder_combout\);

-- Location: FF_X66_Y4_N10
\REGA2|dffs[3]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	d => \REGA2|dffs[3]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA2|dffs[3]~DUPLICATE_q\);

-- Location: LABCELL_X66_Y4_N0
\REGA2|dffs[0]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \REGA2|dffs[0]~feeder_combout\ = \REGA2|dffs[3]~DUPLICATE_q\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \REGA2|ALT_INV_dffs[3]~DUPLICATE_q\,
	combout => \REGA2|dffs[0]~feeder_combout\);

-- Location: FF_X66_Y4_N1
\REGA2|dffs[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	d => \REGA2|dffs[0]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA2|dffs\(0));

-- Location: IOIBUF_X36_Y0_N1
\S2[5]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_S2(5),
	o => \S2[5]~input_o\);

-- Location: IOIBUF_X34_Y0_N41
\S2[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_S2(1),
	o => \S2[1]~input_o\);

-- Location: LABCELL_X66_Y4_N57
\Mux46~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux46~0_combout\ = ( !\REGDIR|dffs\(30) & ( \S2[1]~input_o\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000000000000000000001111000011110000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_S2[1]~input_o\,
	datae => \REGDIR|ALT_INV_dffs\(30),
	combout => \Mux46~0_combout\);

-- Location: FF_X67_Y4_N2
\REGS2|dffs[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	asdata => \Mux46~0_combout\,
	sload => VCC,
	ena => \REGS2|dffs[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGS2|dffs\(1));

-- Location: IOIBUF_X26_Y0_N41
\S2[3]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_S2(3),
	o => \S2[3]~input_o\);

-- Location: LABCELL_X67_Y4_N42
\Mux44~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux44~0_combout\ = ( \REGDIR|dffs\(30) & ( \REGS2|dffs\(1) ) ) # ( !\REGDIR|dffs\(30) & ( \S2[3]~input_o\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \REGS2|ALT_INV_dffs\(1),
	datac => \ALT_INV_S2[3]~input_o\,
	dataf => \REGDIR|ALT_INV_dffs\(30),
	combout => \Mux44~0_combout\);

-- Location: FF_X67_Y4_N44
\REGS2|dffs[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux44~0_combout\,
	ena => \REGS2|dffs[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGS2|dffs\(3));

-- Location: LABCELL_X67_Y4_N18
\Mux42~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux42~0_combout\ = ( \REGS2|dffs\(3) & ( (\REGDIR|dffs\(30)) # (\S2[5]~input_o\) ) ) # ( !\REGS2|dffs\(3) & ( (\S2[5]~input_o\ & !\REGDIR|dffs\(30)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011000000110000001100000011000000111111001111110011111100111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_S2[5]~input_o\,
	datac => \REGDIR|ALT_INV_dffs\(30),
	dataf => \REGS2|ALT_INV_dffs\(3),
	combout => \Mux42~0_combout\);

-- Location: FF_X67_Y4_N26
\REGS2|dffs[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	asdata => \Mux42~0_combout\,
	sload => VCC,
	ena => \REGS2|dffs[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGS2|dffs\(5));

-- Location: IOIBUF_X89_Y38_N55
\S2[7]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_S2(7),
	o => \S2[7]~input_o\);

-- Location: LABCELL_X67_Y4_N57
\Mux40~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Mux40~0_combout\ = ( \REGDIR|dffs\(30) & ( \REGS2|dffs\(5) ) ) # ( !\REGDIR|dffs\(30) & ( \S2[7]~input_o\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \REGS2|ALT_INV_dffs\(5),
	datac => \ALT_INV_S2[7]~input_o\,
	dataf => \REGDIR|ALT_INV_dffs\(30),
	combout => \Mux40~0_combout\);

-- Location: FF_X67_Y4_N59
\REGS2|dffs[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLK~inputCLKENA0_outclk\,
	d => \Mux40~0_combout\,
	ena => \REGS2|dffs[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGS2|dffs\(7));

-- Location: LABCELL_X66_Y4_N27
\DA2[19]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \DA2[19]~1_combout\ = ( \REGS2|dffs\(7) & ( \REGDIR|dffs\(30) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \REGS2|ALT_INV_dffs\(7),
	dataf => \REGDIR|ALT_INV_dffs\(30),
	combout => \DA2[19]~1_combout\);

-- Location: FF_X66_Y4_N29
\REGA2|dffs[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	d => \DA2[19]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA2|dffs\(19));

-- Location: MLABCELL_X65_Y4_N30
\REGA2|dffs[16]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \REGA2|dffs[16]~feeder_combout\ = \REGA2|dffs\(19)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \REGA2|ALT_INV_dffs\(19),
	combout => \REGA2|dffs[16]~feeder_combout\);

-- Location: FF_X65_Y4_N32
\REGA2|dffs[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	d => \REGA2|dffs[16]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA2|dffs\(16));

-- Location: MLABCELL_X65_Y4_N12
\REGA2|dffs[13]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \REGA2|dffs[13]~feeder_combout\ = \REGA2|dffs\(16)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \REGA2|ALT_INV_dffs\(16),
	combout => \REGA2|dffs[13]~feeder_combout\);

-- Location: FF_X65_Y4_N13
\REGA2|dffs[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	d => \REGA2|dffs[13]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA2|dffs\(13));

-- Location: MLABCELL_X65_Y4_N57
\REGA2|dffs[10]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \REGA2|dffs[10]~feeder_combout\ = \REGA2|dffs\(13)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \REGA2|ALT_INV_dffs\(13),
	combout => \REGA2|dffs[10]~feeder_combout\);

-- Location: FF_X65_Y4_N59
\REGA2|dffs[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	d => \REGA2|dffs[10]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA2|dffs\(10));

-- Location: MLABCELL_X65_Y4_N15
\REGA2|dffs[7]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \REGA2|dffs[7]~feeder_combout\ = \REGA2|dffs\(10)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \REGA2|ALT_INV_dffs\(10),
	combout => \REGA2|dffs[7]~feeder_combout\);

-- Location: FF_X65_Y4_N17
\REGA2|dffs[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	d => \REGA2|dffs[7]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA2|dffs\(7));

-- Location: MLABCELL_X65_Y4_N18
\REGA2|dffs[4]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \REGA2|dffs[4]~feeder_combout\ = \REGA2|dffs\(7)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \REGA2|ALT_INV_dffs\(7),
	combout => \REGA2|dffs[4]~feeder_combout\);

-- Location: FF_X65_Y4_N19
\REGA2|dffs[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	d => \REGA2|dffs[4]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA2|dffs\(4));

-- Location: LABCELL_X66_Y4_N12
\REGA2|dffs[1]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \REGA2|dffs[1]~feeder_combout\ = \REGA2|dffs\(4)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \REGA2|ALT_INV_dffs\(4),
	combout => \REGA2|dffs[1]~feeder_combout\);

-- Location: FF_X66_Y4_N13
\REGA2|dffs[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	d => \REGA2|dffs[1]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA2|dffs\(1));

-- Location: LABCELL_X66_Y4_N33
\REGA2|dffs[20]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \REGA2|dffs[20]~0_combout\ = ( !\REGDIR|dffs\(30) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \REGDIR|ALT_INV_dffs\(30),
	combout => \REGA2|dffs[20]~0_combout\);

-- Location: FF_X66_Y4_N35
\REGA2|dffs[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	d => \REGA2|dffs[20]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA2|dffs\(20));

-- Location: FF_X66_Y4_N25
\REGA2|dffs[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	asdata => \REGA2|dffs\(20),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA2|dffs\(17));

-- Location: MLABCELL_X65_Y4_N45
\REGA2|dffs[14]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \REGA2|dffs[14]~feeder_combout\ = ( \REGA2|dffs\(17) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \REGA2|ALT_INV_dffs\(17),
	combout => \REGA2|dffs[14]~feeder_combout\);

-- Location: FF_X65_Y4_N47
\REGA2|dffs[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	d => \REGA2|dffs[14]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA2|dffs\(14));

-- Location: MLABCELL_X65_Y4_N24
\REGA2|dffs[11]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \REGA2|dffs[11]~feeder_combout\ = \REGA2|dffs\(14)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \REGA2|ALT_INV_dffs\(14),
	combout => \REGA2|dffs[11]~feeder_combout\);

-- Location: FF_X65_Y4_N25
\REGA2|dffs[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	d => \REGA2|dffs[11]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA2|dffs\(11));

-- Location: MLABCELL_X65_Y4_N54
\REGA2|dffs[8]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \REGA2|dffs[8]~feeder_combout\ = \REGA2|dffs\(11)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \REGA2|ALT_INV_dffs\(11),
	combout => \REGA2|dffs[8]~feeder_combout\);

-- Location: FF_X65_Y4_N55
\REGA2|dffs[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	d => \REGA2|dffs[8]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA2|dffs\(8));

-- Location: MLABCELL_X65_Y4_N6
\REGA2|dffs[5]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \REGA2|dffs[5]~feeder_combout\ = ( \REGA2|dffs\(8) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \REGA2|ALT_INV_dffs\(8),
	combout => \REGA2|dffs[5]~feeder_combout\);

-- Location: FF_X65_Y4_N7
\REGA2|dffs[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	d => \REGA2|dffs[5]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA2|dffs\(5));

-- Location: MLABCELL_X65_Y4_N33
\REGA2|dffs[2]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \REGA2|dffs[2]~feeder_combout\ = \REGA2|dffs\(5)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \REGA2|ALT_INV_dffs\(5),
	combout => \REGA2|dffs[2]~feeder_combout\);

-- Location: FF_X65_Y4_N34
\REGA2|dffs[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	d => \REGA2|dffs[2]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA2|dffs\(2));

-- Location: FF_X66_Y4_N11
\REGA2|dffs[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	d => \REGA2|dffs[3]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA2|dffs\(3));

-- Location: FF_X65_Y4_N46
\REGA2|dffs[14]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLKP~combout\,
	d => \REGA2|dffs[14]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \REGA2|dffs[14]~DUPLICATE_q\);

-- Location: LABCELL_X71_Y7_N0
\~QUARTUS_CREATED_GND~I\ : cyclonev_lcell_comb
-- Equation(s):

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
;
END structure;


