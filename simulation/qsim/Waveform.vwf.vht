-- Copyright (C) 2018  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details.

-- *****************************************************************************
-- This file contains a Vhdl test bench with test vectors .The test vectors     
-- are exported from a vector file in the Quartus Waveform Editor and apply to  
-- the top level entity of the current Quartus project .The user can use this   
-- testbench to simulate his design using a third-party simulation tool .       
-- *****************************************************************************
-- Generated on "09/01/2020 18:07:54"
                                                             
-- Vhdl Test Bench(with test vectors) for design  :          NeedleWunsch
-- 
-- Simulation tool : 3rd Party
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY NeedleWunsch_vhd_vec_tst IS
END NeedleWunsch_vhd_vec_tst;
ARCHITECTURE NeedleWunsch_arch OF NeedleWunsch_vhd_vec_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL CLK : STD_LOGIC;
SIGNAL OUTMAT : STD_LOGIC_VECTOR(95 DOWNTO 0);
SIGNAL S1I : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL S2I : STD_LOGIC_VECTOR(7 DOWNTO 0);
COMPONENT NeedleWunsch
	PORT (
	CLK : IN STD_LOGIC;
	OUTMAT : BUFFER STD_LOGIC_VECTOR(95 DOWNTO 0);
	S1I : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
	S2I : IN STD_LOGIC_VECTOR(7 DOWNTO 0)
	);
END COMPONENT;
BEGIN
	i1 : NeedleWunsch
	PORT MAP (
-- list connections between master ports and signals
	CLK => CLK,
	OUTMAT => OUTMAT,
	S1I => S1I,
	S2I => S2I
	);

-- CLK
t_prcs_CLK: PROCESS
BEGIN
LOOP
	CLK <= '0';
	WAIT FOR 5000 ps;
	CLK <= '1';
	WAIT FOR 5000 ps;
	IF (NOW >= 1000000 ps) THEN WAIT; END IF;
END LOOP;
END PROCESS t_prcs_CLK;
-- S1I[7]
t_prcs_S1I_7: PROCESS
BEGIN
	S1I(7) <= '1';
WAIT;
END PROCESS t_prcs_S1I_7;
-- S1I[6]
t_prcs_S1I_6: PROCESS
BEGIN
	S1I(6) <= '1';
WAIT;
END PROCESS t_prcs_S1I_6;
-- S1I[5]
t_prcs_S1I_5: PROCESS
BEGIN
	S1I(5) <= '0';
WAIT;
END PROCESS t_prcs_S1I_5;
-- S1I[4]
t_prcs_S1I_4: PROCESS
BEGIN
	S1I(4) <= '1';
WAIT;
END PROCESS t_prcs_S1I_4;
-- S1I[3]
t_prcs_S1I_3: PROCESS
BEGIN
	S1I(3) <= '1';
WAIT;
END PROCESS t_prcs_S1I_3;
-- S1I[2]
t_prcs_S1I_2: PROCESS
BEGIN
	S1I(2) <= '0';
WAIT;
END PROCESS t_prcs_S1I_2;
-- S1I[1]
t_prcs_S1I_1: PROCESS
BEGIN
	S1I(1) <= '0';
WAIT;
END PROCESS t_prcs_S1I_1;
-- S1I[0]
t_prcs_S1I_0: PROCESS
BEGIN
	S1I(0) <= '0';
WAIT;
END PROCESS t_prcs_S1I_0;
-- S2I[7]
t_prcs_S2I_7: PROCESS
BEGIN
	S2I(7) <= '1';
WAIT;
END PROCESS t_prcs_S2I_7;
-- S2I[6]
t_prcs_S2I_6: PROCESS
BEGIN
	S2I(6) <= '1';
WAIT;
END PROCESS t_prcs_S2I_6;
-- S2I[5]
t_prcs_S2I_5: PROCESS
BEGIN
	S2I(5) <= '0';
WAIT;
END PROCESS t_prcs_S2I_5;
-- S2I[4]
t_prcs_S2I_4: PROCESS
BEGIN
	S2I(4) <= '1';
WAIT;
END PROCESS t_prcs_S2I_4;
-- S2I[3]
t_prcs_S2I_3: PROCESS
BEGIN
	S2I(3) <= '1';
WAIT;
END PROCESS t_prcs_S2I_3;
-- S2I[2]
t_prcs_S2I_2: PROCESS
BEGIN
	S2I(2) <= '0';
WAIT;
END PROCESS t_prcs_S2I_2;
-- S2I[1]
t_prcs_S2I_1: PROCESS
BEGIN
	S2I(1) <= '0';
WAIT;
END PROCESS t_prcs_S2I_1;
-- S2I[0]
t_prcs_S2I_0: PROCESS
BEGIN
	S2I(0) <= '0';
WAIT;
END PROCESS t_prcs_S2I_0;
END NeedleWunsch_arch;
