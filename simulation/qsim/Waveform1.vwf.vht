-- Copyright (C) 2018  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details.

-- *****************************************************************************
-- This file contains a Vhdl test bench with test vectors .The test vectors     
-- are exported from a vector file in the Quartus Waveform Editor and apply to  
-- the top level entity of the current Quartus project .The user can use this   
-- testbench to simulate his design using a third-party simulation tool .       
-- *****************************************************************************
-- Generated on "09/06/2020 12:47:30"
                                                             
-- Vhdl Test Bench(with test vectors) for design  :          Traceback
-- 
-- Simulation tool : 3rd Party
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY Traceback_vhd_vec_tst IS
END Traceback_vhd_vec_tst;
ARCHITECTURE Traceback_arch OF Traceback_vhd_vec_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL A1 : STD_LOGIC_VECTOR(20 DOWNTO 0);
SIGNAL A2 : STD_LOGIC_VECTOR(20 DOWNTO 0);
SIGNAL CLK : STD_LOGIC;
SIGNAL ODIR : STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL OS1 : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL OS2 : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL S1 : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL S2 : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL TB : STD_LOGIC_VECTOR(31 DOWNTO 0);
COMPONENT Traceback
	PORT (
	A1 : OUT STD_LOGIC_VECTOR(20 DOWNTO 0);
	A2 : OUT STD_LOGIC_VECTOR(20 DOWNTO 0);
	CLK : IN STD_LOGIC;
	ODIR : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
	OS1 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	OS2 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	S1 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
	S2 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
	TB : IN STD_LOGIC_VECTOR(31 DOWNTO 0)
	);
END COMPONENT;
BEGIN
	i1 : Traceback
	PORT MAP (
-- list connections between master ports and signals
	A1 => A1,
	A2 => A2,
	CLK => CLK,
	ODIR => ODIR,
	OS1 => OS1,
	OS2 => OS2,
	S1 => S1,
	S2 => S2,
	TB => TB
	);

-- CLK
t_prcs_CLK: PROCESS
BEGIN
LOOP
	CLK <= '0';
	WAIT FOR 5000 ps;
	CLK <= '1';
	WAIT FOR 5000 ps;
	IF (NOW >= 1000000 ps) THEN WAIT; END IF;
END LOOP;
END PROCESS t_prcs_CLK;
-- S1[7]
t_prcs_S1_7: PROCESS
BEGIN
	S1(7) <= '1';
WAIT;
END PROCESS t_prcs_S1_7;
-- S1[6]
t_prcs_S1_6: PROCESS
BEGIN
	S1(6) <= '1';
WAIT;
END PROCESS t_prcs_S1_6;
-- S1[5]
t_prcs_S1_5: PROCESS
BEGIN
	S1(5) <= '1';
WAIT;
END PROCESS t_prcs_S1_5;
-- S1[4]
t_prcs_S1_4: PROCESS
BEGIN
	S1(4) <= '0';
WAIT;
END PROCESS t_prcs_S1_4;
-- S1[3]
t_prcs_S1_3: PROCESS
BEGIN
	S1(3) <= '0';
WAIT;
END PROCESS t_prcs_S1_3;
-- S1[2]
t_prcs_S1_2: PROCESS
BEGIN
	S1(2) <= '1';
WAIT;
END PROCESS t_prcs_S1_2;
-- S1[1]
t_prcs_S1_1: PROCESS
BEGIN
	S1(1) <= '0';
WAIT;
END PROCESS t_prcs_S1_1;
-- S1[0]
t_prcs_S1_0: PROCESS
BEGIN
	S1(0) <= '0';
WAIT;
END PROCESS t_prcs_S1_0;
-- S2[7]
t_prcs_S2_7: PROCESS
BEGIN
	S2(7) <= '1';
WAIT;
END PROCESS t_prcs_S2_7;
-- S2[6]
t_prcs_S2_6: PROCESS
BEGIN
	S2(6) <= '0';
WAIT;
END PROCESS t_prcs_S2_6;
-- S2[5]
t_prcs_S2_5: PROCESS
BEGIN
	S2(5) <= '1';
WAIT;
END PROCESS t_prcs_S2_5;
-- S2[4]
t_prcs_S2_4: PROCESS
BEGIN
	S2(4) <= '1';
WAIT;
END PROCESS t_prcs_S2_4;
-- S2[3]
t_prcs_S2_3: PROCESS
BEGIN
	S2(3) <= '0';
WAIT;
END PROCESS t_prcs_S2_3;
-- S2[2]
t_prcs_S2_2: PROCESS
BEGIN
	S2(2) <= '1';
WAIT;
END PROCESS t_prcs_S2_2;
-- S2[1]
t_prcs_S2_1: PROCESS
BEGIN
	S2(1) <= '0';
WAIT;
END PROCESS t_prcs_S2_1;
-- S2[0]
t_prcs_S2_0: PROCESS
BEGIN
	S2(0) <= '0';
WAIT;
END PROCESS t_prcs_S2_0;

-- TB[31]
t_prcs_TB_31: PROCESS
BEGIN
	TB(31) <= '1';
WAIT;
END PROCESS t_prcs_TB_31;

-- TB[30]
t_prcs_TB_30: PROCESS
BEGIN
	TB(30) <= '1';
WAIT;
END PROCESS t_prcs_TB_30;

-- TB[29]
t_prcs_TB_29: PROCESS
BEGIN
	TB(29) <= '1';
WAIT;
END PROCESS t_prcs_TB_29;

-- TB[28]
t_prcs_TB_28: PROCESS
BEGIN
	TB(28) <= '0';
WAIT;
END PROCESS t_prcs_TB_28;

-- TB[27]
t_prcs_TB_27: PROCESS
BEGIN
	TB(27) <= '0';
WAIT;
END PROCESS t_prcs_TB_27;

-- TB[26]
t_prcs_TB_26: PROCESS
BEGIN
	TB(26) <= '1';
WAIT;
END PROCESS t_prcs_TB_26;

-- TB[25]
t_prcs_TB_25: PROCESS
BEGIN
	TB(25) <= '0';
WAIT;
END PROCESS t_prcs_TB_25;

-- TB[24]
t_prcs_TB_24: PROCESS
BEGIN
	TB(24) <= '1';
WAIT;
END PROCESS t_prcs_TB_24;

-- TB[23]
t_prcs_TB_23: PROCESS
BEGIN
	TB(23) <= '1';
WAIT;
END PROCESS t_prcs_TB_23;

-- TB[22]
t_prcs_TB_22: PROCESS
BEGIN
	TB(22) <= '0';
WAIT;
END PROCESS t_prcs_TB_22;

-- TB[21]
t_prcs_TB_21: PROCESS
BEGIN
	TB(21) <= '1';
WAIT;
END PROCESS t_prcs_TB_21;

-- TB[20]
t_prcs_TB_20: PROCESS
BEGIN
	TB(20) <= '0';
WAIT;
END PROCESS t_prcs_TB_20;

-- TB[19]
t_prcs_TB_19: PROCESS
BEGIN
	TB(19) <= '0';
WAIT;
END PROCESS t_prcs_TB_19;

-- TB[18]
t_prcs_TB_18: PROCESS
BEGIN
	TB(18) <= '1';
WAIT;
END PROCESS t_prcs_TB_18;

-- TB[17]
t_prcs_TB_17: PROCESS
BEGIN
	TB(17) <= '0';
WAIT;
END PROCESS t_prcs_TB_17;

-- TB[16]
t_prcs_TB_16: PROCESS
BEGIN
	TB(16) <= '1';
WAIT;
END PROCESS t_prcs_TB_16;

-- TB[15]
t_prcs_TB_15: PROCESS
BEGIN
	TB(15) <= '1';
WAIT;
END PROCESS t_prcs_TB_15;

-- TB[14]
t_prcs_TB_14: PROCESS
BEGIN
	TB(14) <= '0';
WAIT;
END PROCESS t_prcs_TB_14;

-- TB[13]
t_prcs_TB_13: PROCESS
BEGIN
	TB(13) <= '1';
WAIT;
END PROCESS t_prcs_TB_13;

-- TB[12]
t_prcs_TB_12: PROCESS
BEGIN
	TB(12) <= '0';
WAIT;
END PROCESS t_prcs_TB_12;

-- TB[11]
t_prcs_TB_11: PROCESS
BEGIN
	TB(11) <= '1';
WAIT;
END PROCESS t_prcs_TB_11;

-- TB[10]
t_prcs_TB_10: PROCESS
BEGIN
	TB(10) <= '1';
WAIT;
END PROCESS t_prcs_TB_10;

-- TB[9]
t_prcs_TB_9: PROCESS
BEGIN
	TB(9) <= '0';
WAIT;
END PROCESS t_prcs_TB_9;

-- TB[8]
t_prcs_TB_8: PROCESS
BEGIN
	TB(8) <= '1';
WAIT;
END PROCESS t_prcs_TB_8;

-- TB[7]
t_prcs_TB_7: PROCESS
BEGIN
	TB(7) <= '1';
WAIT;
END PROCESS t_prcs_TB_7;

-- TB[6]
t_prcs_TB_6: PROCESS
BEGIN
	TB(6) <= '0';
WAIT;
END PROCESS t_prcs_TB_6;

-- TB[5]
t_prcs_TB_5: PROCESS
BEGIN
	TB(5) <= '1';
WAIT;
END PROCESS t_prcs_TB_5;

-- TB[4]
t_prcs_TB_4: PROCESS
BEGIN
	TB(4) <= '0';
WAIT;
END PROCESS t_prcs_TB_4;

-- TB[3]
t_prcs_TB_3: PROCESS
BEGIN
	TB(3) <= '1';
WAIT;
END PROCESS t_prcs_TB_3;

-- TB[2]
t_prcs_TB_2: PROCESS
BEGIN
	TB(2) <= '0';
WAIT;
END PROCESS t_prcs_TB_2;

-- TB[1]
t_prcs_TB_1: PROCESS
BEGIN
	TB(1) <= '1';
WAIT;
END PROCESS t_prcs_TB_1;

-- TB[0]
t_prcs_TB_0: PROCESS
BEGIN
	TB(0) <= '1';
WAIT;
END PROCESS t_prcs_TB_0;
END Traceback_arch;
