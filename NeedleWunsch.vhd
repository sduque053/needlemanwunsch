library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
LIBRARY lpm;
USE lpm.lpm_components.all;

entity NeedleWunsch is
	port(
		CLK		: IN  std_logic;
		S1I,S2I	: IN  std_logic_vector(2*N-1 	downto 0);
		OUTMAT	: OUT std_logic_vector(95 		downto 0);
		TB		: OUT std_logic_vector(31 		downto 0)
	);
end entity;

architecture RTL of NeedleWunsch is

	signal INIT 			: std_logic_vector(19 downto 0):="11001101111011110000";
	signal D0S,D1S,D2S	: std_logic_vector(63 downto 0);
	signal MAT				: std_logic_vector(95 downto 0);
	
	component PE is
		port(
			CLK			:IN  std_logic;
			S1,S2			:IN  std_logic_vector(1 downto 0);
			D0,D1,D2		:IN  std_logic_vector(3 downto 0);
			DIR			:OUT std_logic_vector(1 downto 0);
			D3				:OUT std_logic_vector(3 downto 0)
		);
	end component;

	begin
	
		COLS: for I in 0 to 3 generate
			ROWS: for J in 0 to 3 generate
				UNIT: PE port map(
					CLK,S1I(1+2*I downto 0+2*I),S2I(1+2*J downto 0+2*J),
					D0S(3+4*I+16*J downto 0+4*I+16*J),D1S(3+4*I+16*J downto 0+4*I+16*J),
					D2S(3+4*I+16*J downto 0+4*I+16*J),MAT(5+6*I+24*J downto 4+6*I+24*J),
					MAT(3+6*I+24*J downto 0+6*I+24*J)
				);
				
				TB(1+2*I+8*J downto 0+2*I+8*J)<=MAT(5+6*I+24*J downto 4+6*I+24*J);
				
			end generate ROWS;
		end generate COLS;
		
		
		
		COLC: for I in 1 to 3 generate
			ROWC: for J in 1 to 3 generate
				D0S(3+4*I+16*J downto 0+4*I+16*J)<=MAT(3+6*(I-1)+24*(J-1) 	downto 0+6*(I-1)+24*(J-1));
				D1S(3+4*I+16*J downto 0+4*I+16*J)<=MAT(3+6*I    +24*(J-1)   downto 0+6*I    +24*(J-1));
				D2S(3+4*I+16*J downto 0+4*I+16*J)<=MAT(3+6*(I-1)+24*J   		downto 0+6*(I-1)+24*J);
			end generate ROWC;
		end generate COLC;
		
		COL1: for I in 1 to 3 generate
			D2S(3+ 4*I downto 0+ 4*I)<=MAT(3+ 6*(I-1) downto 0+ 6*(I-1));
		end generate COL1;
		
		ROW1: for J in 1 to 3 generate
			D1S(3+16*J downto 0+16*J)<=MAT(3+24*(J-1) downto 0+24*(J-1));
		end generate ROW1;
		
		OUTMAT<=MAT;
			
		D0S(15 downto  0)<=INIT(15 downto  0);
		D0S(19 downto 16)<=INIT( 7 downto  4);
		D0S(35 downto 32)<=INIT(11 downto  8);
		D0S(51 downto 48)<=INIT(15 downto 12);

		D1S(15 downto  0)<=INIT(19 downto  4);
		
		D2S( 3 downto  0)<=INIT( 7 downto  4);
		D2S(19 downto 16)<=INIT(11 downto  8);
		D2S(35 downto 32)<=INIT(15 downto 12);
		D2S(51 downto 48)<=INIT(19 downto 16);
		
		
		
end RTL;
